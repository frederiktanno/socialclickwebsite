**SOCIAL CLICK WEBSITE**

Prototype for a social work/volunteering platform website, where volunteering projects across Indonesia are gathered
Create your own project and track its progress, while making it open for others to apply as volunteers
Or Join an existing project made and managed by other organisations or individuals, where you can meet new people and experience new things

With SocialClick, organisations can create, publish, and manage their projects, while also accepting offers from individuals who are looking to 
join as volunteers in their projects.

Individuals can also create and manage their own project or join other projects created by other individuals or organizations, track their list of 
applied projects, and track the amount of hours they had accumulated in volunteering projects.

This Repository contains the back-end module source code for this application, which is developed mainly using Java Spring Framework, and MySQL
as the database system.
