/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.controllers;
import com.mycompany.socialclickwebsite.models.Organization;
import com.mycompany.socialclickwebsite.models.Project;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.mycompany.socialclickwebsite.models.Users;
import com.mycompany.socialclickwebsite.services.AdminServices;
import com.mycompany.socialclickwebsite.services.EmailServices;
import com.mycompany.socialclickwebsite.services.OrganizationServices;
import com.mycompany.socialclickwebsite.services.UserServices;
import com.mycompany.socialclickwebsite.services.JWTUniversalValidationServices;
import com.mycompany.socialclickwebsite.services.ProjectServices;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.context.request.WebRequest;
/**
 *
 * @author ASUS
 */
@RestController
@ComponentScan("com.mycompany.socialclickwebsite.services")
public class AdminController {
    @Autowired
    private UserServices uServices;
    
    @Autowired
    private OrganizationServices orgServices;
    
    @Autowired
    private JWTUniversalValidationServices jwtServices;
    
    @Autowired
    private ProjectServices pServices;
    
    @Autowired
    private AdminServices aServices;
    
    @RequestMapping(value="/loginAdmin",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String>loginAdminPost(@RequestBody Map<String,Object>data,UriComponentsBuilder ucBuilder ){
        String email=data.get("username").toString();
        String password=data.get("password").toString();
        System.out.println(email+" "+password);
        String valid=aServices.loginAdmin(email,password);//authenticate user's email, password
        //and create token
        System.out.println(valid);
        if(valid.contains("error")|| "invalid email or password".equals(valid)||"admin not found".equals(valid)){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        System.out.println("found");
        //String id=uServices.returnId(email);//get user's ID
        //String[]array={valid,id};
        /*String token=uServices.getToken(id);//return token
       String validate= uServices.validateUser(token, id);//validates user token
       if(validate!="valid"){
           return new ResponseEntity(HttpStatus.UNAUTHORIZED);
       }*/
        return new ResponseEntity<>(valid, HttpStatus.OK);
    }
    
    @RequestMapping(value="/admin/controlpanel/users",method=RequestMethod.GET)
    public ResponseEntity<List<Users>>getControlPanelUsers(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){
        
        String validate=jwtServices.validateAdmin(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<Users>users=uServices.getUser();
        if(users==null){
            return new ResponseEntity("empty",HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(users,HttpStatus.OK);
        
    }
    
    @RequestMapping(value="/admin/controlpanel/org",method=RequestMethod.GET)
    public ResponseEntity<List<Organization>>getControlPanelOrg(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){
        
        String validate=jwtServices.validateAdmin(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<Organization>org=orgServices.getOrganization();
        if(org==null){
            return new ResponseEntity("empty",HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(org,HttpStatus.OK);
        
    }
    
    @RequestMapping(value="/admin/controlpanel/projects",method=RequestMethod.GET)
    public ResponseEntity<List<Organization>>getControlPanelProjects(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){
        
        String validate=jwtServices.validateAdmin(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<Project>proj=pServices.getProject();
        if(proj==null){
            return new ResponseEntity("empty",HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(proj,HttpStatus.OK);
        
    }
    
    @RequestMapping(value="/admin/controlpanel/user/{id}",method=RequestMethod.GET)
    public ResponseEntity<Users>getControlPanelUser(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @PathVariable("id")String idUser){
        
        String validate=jwtServices.validateAdmin(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Users user=uServices.findById(idUser);
        if(user==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        System.out.println(idUser);
        //user.setPassword("");
        //System.out.println(validate);
        return new ResponseEntity(user,HttpStatus.OK);
    }
    
    @RequestMapping(value="/admin/controlpanel/user/{id}/ban",method=RequestMethod.POST)
    public ResponseEntity<String>banUser(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @PathVariable("id")String idUser){
        
        String validate=jwtServices.validateAdmin(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        String ban=aServices.banUser(idUser);
        if(ban.contains("error")){
            return new ResponseEntity(HttpStatus.NOT_MODIFIED);
        }
        //user.setPassword("");
        //System.out.println(validate);
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value="/admin/controlpanel/org/{id}",method=RequestMethod.GET)
    public ResponseEntity<Organization>getControlPanelOrganization(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @PathVariable("id")String idOrg){
        
        String validate=jwtServices.validateAdmin(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Organization user=orgServices.getOrganizationById(idOrg);
        if(user==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        System.out.println(idOrg);
        //user.setPassword("");
        //System.out.println(validate);
        return new ResponseEntity(user,HttpStatus.OK);
    }
    
    @RequestMapping(value="/admin/controlpanel/project/{id}",method=RequestMethod.GET)
    public ResponseEntity<Organization>getControlPanelProject(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @PathVariable("id")String idProject){
        
        String validate=jwtServices.validateAdmin(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Project project=pServices.getProjectById(idProject);
        if(project==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        System.out.println(idProject);
        //user.setPassword("");
        //System.out.println(validate);
        return new ResponseEntity(project,HttpStatus.OK);
    }
    
}
