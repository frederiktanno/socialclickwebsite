/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.controllers;

/**
 *
 * @author ASUS
 */
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mycompany.socialclickwebsite.models.Organization;
import com.mycompany.socialclickwebsite.models.Project;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.mycompany.socialclickwebsite.models.Users;
import com.mycompany.socialclickwebsite.services.EmailServices;
import com.mycompany.socialclickwebsite.services.OrganizationServices;
import com.mycompany.socialclickwebsite.services.UserServices;
import com.mycompany.socialclickwebsite.services.JWTUniversalValidationServices;
import com.mycompany.socialclickwebsite.services.ProjectServices;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.context.request.WebRequest;

/*@CrossOrigin(origins="*",maxAge=3600,allowedHeaders="origin, content-type, accept, authorization, token, id"
,allowCredentials="true")*/
//@CrossOrigin(origins="*",allowCredentials="false",maxAge=3600)
@RestController
@ComponentScan("com.mycompany.socialclickwebsite.services")
public class MainController {
    PasswordHash ph=new PasswordHash(); 
    
    @Autowired
    private ApplicationEventPublisher eventPublisher;
                
    @Autowired
    private UserServices uServices;
    
    @Autowired
    private OrganizationServices orgServices;
    
    @Autowired
    private JWTUniversalValidationServices jwtServices;
    
    @Autowired
    private ProjectServices pServices;
    
    @Autowired
    private EmailServices eServices;
        
    @RequestMapping(method = RequestMethod.OPTIONS)
    public ResponseEntity handle() {
        HttpHeaders header=new HttpHeaders();
        header.setAccessControlAllowOrigin("*");
        return new ResponseEntity(header,HttpStatus.NO_CONTENT);
    }//handles OPTIONS request method for every POST request method
    
    @RequestMapping(value="/",method=RequestMethod.GET)
    public ResponseEntity<Void>homePage(){
        return new ResponseEntity(HttpStatus.OK);
    }
    //@CrossOrigin(origins="http://103.8.79.107:8080",allowCredentials="false")
    @RequestMapping(value="/login",method=RequestMethod.GET)
    public ResponseEntity<Void>loginPage(){
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value="/login/admin",method=RequestMethod.GET)
    public ResponseEntity<Void>loginAdmin(){
        return new ResponseEntity(HttpStatus.OK);
          
    }
    
    @RequestMapping(value="/login/org",method=RequestMethod.GET)
    public ResponseEntity<String>loginOrgPage(){
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value="/login",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String>loginPost(@RequestBody Map<String,Object>data,UriComponentsBuilder ucBuilder ){
        System.out.println("logging in POST");
        String email=data.get("username").toString();
        String password=data.get("password").toString();
        System.out.println(email+" "+password);
        String valid=uServices.loginAuth(email,password);//authenticate user's email, password
        //and create token
        System.out.println(valid);
        if(valid.contains("error")|| "invalid email or password".equals(valid)){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        else if(valid.contains("email not verified")){
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        System.out.println("found");
        //String id=uServices.returnId(email);//get user's ID
        //String[]array={valid,id};
        /*String token=uServices.getToken(id);//return token
       String validate= uServices.validateUser(token, id);//validates user token
       if(validate!="valid"){
           return new ResponseEntity(HttpStatus.UNAUTHORIZED);
       }*/
        System.out.println(valid);
        return new ResponseEntity(valid,HttpStatus.OK);
    }
    
    
    
    @RequestMapping(value="/token",method=RequestMethod.GET)
    public ResponseEntity<String>test(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){
        String validate=jwtServices.validateToken(token, id);
        /*if(!"valid".equals(validate)){
           System.out.println("claim not valid is"+validate); 
           return new ResponseEntity(false,HttpStatus.UNAUTHORIZED);
       }*/
        System.out.println("claim is"+validate);
        return new ResponseEntity(true,HttpStatus.OK);
    }//test function
    
    /*@RequestMapping(name="/test",method=RequestMethod.POST)
    public ResponseEntity<String>testUser(WebRequest request,@RequestBody Users data){
        System.out.println(request.getContextPath());
        System.out.println(request.getLocale());
        //String email=eServices.sendEmail("abc");
        //if("error".equals(email)){
        //    return new ResponseEntity(HttpStatus.BAD_REQUEST);
        //}
        try{
           String url=request.getContextPath();
           eventPublisher.publishEvent(new OnRegistrationCompleteEvent(data,request.getLocale(),url));//generate validation
           //token here and send email
       }
       catch(Exception ex){
           return new ResponseEntity("email error",HttpStatus.CONTINUE);
       }
        return new ResponseEntity(HttpStatus.OK);
    }*/
    @RequestMapping(name="/test",method=RequestMethod.GET)
    public ResponseEntity<Users>testUser(){
        Users user=uServices.findById("1");
        if(user==null){
            return new ResponseEntity(user,HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(user,HttpStatus.OK);
    }
    
    @RequestMapping(value="/login/org",method=RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String>loginOrgPost(@RequestBody Map<String,Object>data,UriComponentsBuilder ucBuilder ){
        HttpHeaders header=new HttpHeaders();
        System.out.println("a");
        String email=data.get("username").toString();
        String password=data.get("password").toString();
        String token=orgServices.loginAuth(email,password);
        System.out.println(email +" " + password);
        if(token.contains("error")|| "invalid email or password".equals(token)){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        //String id=orgServices.returnId(email);
        //String[]array={token,id};
        return new ResponseEntity<>(token,HttpStatus.OK);
    }
    
    //check for email and id number duplicates
    @RequestMapping(value="/register/check",method=RequestMethod.POST)
    public ResponseEntity<String>check(@RequestBody String json){
        ObjectMapper objectMap=new ObjectMapper();
        String email,idnum,dateofbirth;
         try {
            //retrieve json value "email" and "idnum" from request body to string
            JsonNode jsonNode=objectMap.readTree(json);
            email=jsonNode.get("email").asText();
            idnum=jsonNode.get("idnum").asText();
            dateofbirth=jsonNode.get("dateofbirth").asText();
            if(uServices.isUserExist(email)||uServices.checkIdNum(idnum)){
                System.out.println("A User with email " + email + " already exist");
                return new ResponseEntity("email already exist or ID with this idnumber already exists",
                        HttpStatus.CONFLICT);
            }
            if(uServices.checkDOB(dateofbirth).equals("too old")||uServices.checkDOB(dateofbirth).equals("too young")){
                return new ResponseEntity("invalid date of birth",HttpStatus.NOT_ACCEPTABLE);
            }
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex) {
           System.out.println(ex.toString());
           return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
    
    @RequestMapping(value="/register",method=RequestMethod.POST)
    public ResponseEntity<String>register(@RequestBody Users data,
            UriComponentsBuilder ucBuilder,WebRequest request){
        System.out.println(data.getId());
        String email=data.getEmail();
        System.out.println(email);
        
        System.out.println(data.getAddress() + " " + data.getfname());
        String insert = uServices.insertData(data);
        //String token=uServices.regisAuth(data);//add user data into the database
        if(insert.contains("error")==true){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        /*
            Apparently i have to use the Spring ApplicationEventPublisher for email validation
            'cause i was told i cant just invoke functions to do so in the controller
            it has to be triggered by an event-listener pair for some reason
            hence the 2 new event and listener files in this directory
        */
       try{
           String url=request.getContextPath();
           eventPublisher.publishEvent(new OnRegistrationCompleteEvent(data,request.getLocale(),url));//generate validation
           //token here and send email
       }
       catch(Exception ex){
           return new ResponseEntity<>("email error",HttpStatus.CONTINUE);
       }
        //String id=uServices.returnId(data.getEmail());
        //String[]array={token,id};
        return new ResponseEntity<>(insert, HttpStatus.CREATED);
        
    }
    
    @RequestMapping(value="/emailValidation/{token}/{id}",method=RequestMethod.GET)
    public ResponseEntity<String>emailValidation(@PathVariable(value="token")String validtoken,
                                                 @PathVariable(value="id")long id){
        System.out.println(validtoken);
        System.out.println(id);
        String valid=uServices.validateValidationToken(validtoken,id);
        if("error:token expired".equals(valid)){
            return new ResponseEntity("token expired",HttpStatus.FORBIDDEN);
        }
        if(valid.contains("error")==true){
            return new ResponseEntity("token error",HttpStatus.BAD_REQUEST);
        }
        //generate a token for user authentication
        String token=uServices.createToken(id);
        if(token.contains("error")==true){
            return new ResponseEntity("error generating token",HttpStatus.BAD_REQUEST);
        }
        System.out.println(token);
        return new ResponseEntity(token,HttpStatus.OK);
    }
    
    
    @RequestMapping(value="/register/org",method=RequestMethod.POST)
    public ResponseEntity<String>registerOrg(@RequestBody Organization data,
            UriComponentsBuilder ucBuilder){
        //String email=data.getEmail();
        if(orgServices.isOrgExist(data)){
            System.out.println("An Organization with email " + data.getEmail() + "or organization with name"+ data.getName()+" already exist");
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
        orgServices.saveOrganization(data);
        String token=orgServices.regisAuth(data);
        //String id=orgServices.returnId(data.getEmail());
        //String[]array={token,id};
        HttpHeaders header=new HttpHeaders();
        return new ResponseEntity<>(token, HttpStatus.CREATED);
        
    }
    
    @RequestMapping(value="/logout",method=RequestMethod.GET)
    public ResponseEntity<Void>logout(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){   
        System.out.println(token + id);
        uServices.logout(token, id);
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value="/user/{id}",method=RequestMethod.GET)//for viewing a certain user, available for everyone
    public ResponseEntity<Users>getSelectedUser(@PathVariable("id")String idUser,
                                        @RequestHeader(value="token")String token,
                                        @RequestHeader(value="id")String id){
        String valid=jwtServices.validateToken(token, id);
        if(!"valid".equals(valid)){
            if("token expired".equals(valid)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(valid.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Users user=uServices.findById(idUser);
        if(user==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        System.out.println(idUser);
        user.setPassword("");
        //System.out.println(validate);
        System.out.println(user);
        return new ResponseEntity(user,HttpStatus.OK);
    }
    
    @RequestMapping(value="/user/{id}",method=RequestMethod.POST)//accept or reject a user's proposal to join project
    public ResponseEntity<Void>ApproveRejectUser(@PathVariable("id")String idUser,
                                        @RequestHeader(value="token")String token,
                                        @RequestHeader(value="id")String id){
        String valid=jwtServices.validateToken(token, id);
        if(!"valid".equals(valid)){
            if("token expired".equals(valid)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(valid.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Users user=uServices.findById(idUser);
        if(user==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        System.out.println(idUser);
        //user.setPassword("");
        //System.out.println(validate);
        System.out.println(user);
        return new ResponseEntity(user,HttpStatus.OK);
    }
    
    @RequestMapping(value="/organization/{name}",method=RequestMethod.GET)
    public ResponseEntity<Users>getSelectedOrganization(@PathVariable("name")String name,
                                        @RequestHeader(value="token")String token,
                                        @RequestHeader(value="id")String id){
        String valid=jwtServices.validateToken(token, id);
        if(!"valid".equals(valid)){
            if("token expired".equals(valid)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(valid.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Organization org=orgServices.getOrganizationByName(name);
        if(org==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        System.out.println(name);
        org.setPassword("");
        //System.out.println(validate);
        System.out.println(org);
        return new ResponseEntity(org,HttpStatus.OK);
    }
    
    @RequestMapping(value="/organizations",method=RequestMethod.GET)
    public ResponseEntity<List<Organization>>getOrganizations(
                                        @RequestHeader(value="token")String token,
                                        @RequestHeader(value="id")String id){
        String valid=jwtServices.validateAdmin(token, id);
        if(!"valid".equals(valid)){
            if("token expired".equals(valid)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(valid.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<Organization>orgList=orgServices.getOrganization();
        if(orgList.isEmpty()==true){
            return new ResponseEntity("empty",HttpStatus.OK);
        }
        return new ResponseEntity(orgList,HttpStatus.OK);
    }
    /*
    How to reset password:
    first, user clicks on Forgot Password button. Then, displays a page which contains a form where the user needs to
    insert his/her e-mail for validation. Then, the system sends an e-mail to the user containing a verification token
    The token is only valid for 5 minutes.
    The user then opens the link sent in the e-mail, where it opens a form to replace with a new password
    In the form the e-mail address cannot be changed, and the user only needs to insert a new password
    If the token is expired, the user needs to click on a link to resend the password
    */
    @RequestMapping(value="/forgotPassword",method=RequestMethod.GET)
    public ResponseEntity<Void>getForgotPassword(){
        return new ResponseEntity(HttpStatus.OK);
    }
    @RequestMapping(value="/forgotPassword",method=RequestMethod.POST)
    public ResponseEntity<String>postForgotPassword(@RequestBody String json
                                                     ,WebRequest request){
        ObjectMapper objectMap=new ObjectMapper();
        String email;
        try {
            //retrieve json value "email" from request body to string
            JsonNode jsonNode=objectMap.readTree(json);
            email=jsonNode.get("email").asText();
            System.out.println(email);
            Users user=uServices.findByName(email);
            if(user==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            try{
                String url=request.getContextPath();
                eventPublisher.publishEvent(new OnForgotPasswordEvent(user,request.getLocale(),url));
                //generate change password token here and send email
            }
            catch(Exception ex){
                System.out.println(ex.toString());
                return new ResponseEntity<>("email error",HttpStatus.CONTINUE);
            }
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex) {
           System.out.println(ex.toString());
           return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }       
    }
    
    @RequestMapping(value="/reset/{token}/{id}",method=RequestMethod.GET)
    public ResponseEntity<String>resetPassword(@PathVariable(value="token")String token,@PathVariable(value="id")long id){
        String valid=uServices.validateResetToken(token, id);
        if("error:token expired".equals(valid)){
            return new ResponseEntity("token expired",HttpStatus.FORBIDDEN);
        }
        else if(valid.contains("error")==true){
            return new ResponseEntity("token error",HttpStatus.BAD_REQUEST);
        }
        Users user=uServices.findById(id+"");
        if(user==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        String email=user.getEmail();//retrieve user's email address to be displayed in the reset password form
        //based on the id value supplied in the path variable
        return new ResponseEntity(email,HttpStatus.OK);
    }
    @RequestMapping(value="/reset/{token}/{id}",method=RequestMethod.POST)
    public ResponseEntity<String>resetPasswordPost(@PathVariable(value="token")String token,@PathVariable(value="id")long id,
                                                 @RequestBody String credentials){
        ObjectMapper objectMap=new ObjectMapper();
        String email,password;
        //check validity of token
        String valid=uServices.validateResetToken(token, id);
        if("error:token expired".equals(valid)){
            return new ResponseEntity("token expired",HttpStatus.FORBIDDEN);
        }
        if(valid.contains("error")==true){
            return new ResponseEntity("token error",HttpStatus.BAD_REQUEST);
        }
        //extract email and password
        try {
            //retrieve json value "email" from request body to string
            JsonNode jsonNode=objectMap.readTree(credentials);
            email=jsonNode.get("email").asText();
            password=jsonNode.get("password").asText();
            System.out.println(password);
            System.out.println(email);
            Users user=uServices.findByName(email);//search for user with the email retrieved from JSON
            if(user==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            String reset=uServices.resetPassword(email, password);//reset user's password
            if(reset.contains("error")){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity("password reset",HttpStatus.OK);
        } 
        catch (Exception ex) {
           System.out.println(ex.toString());
           return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }       
    }
    
    @RequestMapping(value="/org/forgotPassword",method=RequestMethod.GET)
    public ResponseEntity<Void>getForgotPasswordOrg(){
        return new ResponseEntity(HttpStatus.OK);
    }
    @RequestMapping(value="/org/forgotPassword",method=RequestMethod.POST)
    public ResponseEntity<String>postForgotPasswordOrg(@RequestBody String email){
        return new ResponseEntity(HttpStatus.OK);
    }
    
}
