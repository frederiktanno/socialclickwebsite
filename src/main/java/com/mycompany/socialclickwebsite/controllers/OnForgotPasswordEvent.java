/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.controllers;
import com.mycompany.socialclickwebsite.models.Users;
import java.util.Locale;
import org.springframework.context.ApplicationEvent;
/**
 *
 * @author ASUS
 */
public class OnForgotPasswordEvent extends ApplicationEvent{
    private Users data;
    private Locale locale;
    private String url;
    public OnForgotPasswordEvent(Users data,Locale locale, String url){
        super(data);
        this.url=url;
        this.data=data;
        this.locale=locale;
    }
    public Users getUser(){
        return this.data;
    }
    public void setUser(Users data){
        this.data=data;
    }
    public Locale getLocale(){
        return this.locale;
    }
    public void setLocale(Locale locale){
        this.locale=locale;
    }
    public String getUrl(){
        return this.url;
    }
    public void setUrl(String url){
        this.url=url;
    }
}
