/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.controllers;
import com.mycompany.socialclickwebsite.models.Organization;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.mycompany.socialclickwebsite.models.Users;
import com.mycompany.socialclickwebsite.services.JWTUniversalValidationServices;
import com.mycompany.socialclickwebsite.services.OrganizationServices;
import com.mycompany.socialclickwebsite.services.ProjectServices;
import com.mycompany.socialclickwebsite.services.UserServices;
import java.util.Arrays;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
/**
 *
 * @author ASUS
 */
//@CrossOrigin(origins="*",maxAge=3600)
@CrossOrigin
@RestController
@ComponentScan("com.mycompany.socialclickwebsite.services")
public class OrganizationController {
    @Autowired
    private OrganizationServices oServices;
    
    @Autowired
    private JWTUniversalValidationServices jwtServices;
    
    @Autowired
    private ProjectServices pServices;
    
    @RequestMapping(value="/organization/myProfile/{id}",method=RequestMethod.GET)
    public ResponseEntity<Organization>getOrg(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @PathVariable("id") String idOrg){
        String validate=oServices.validateOrg(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Organization org=oServices.getOrganizationById(idOrg);
        if(org==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        org.setPassword("");
        System.out.println(validate);
        return new ResponseEntity(org,HttpStatus.OK);
    }
    
    @RequestMapping(value="/organization/myProfile",method=RequestMethod.PUT)
    public ResponseEntity<String>getOrg(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @RequestBody Organization org){
        
        String validate=oServices.validateOrg(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Organization selected=oServices.getOrganizationById(id);
        if(selected==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        selected.setCemail(org.getCemail());
        selected.setContact(org.getContact());
        selected.setCphone(org.getCphone());
        selected.setDesc(org.getDesc());
        selected.setEmail(org.getEmail());
        if(oServices.checkOrgEmail(org.getEmail(), id)){//check for new email whether it exists on database or not
            return new ResponseEntity("email exists",HttpStatus.CONFLICT);
        }
        selected.setFounded(org.getFounded());
        selected.setName(org.getName());
        if(oServices.checkOrgName(org.getName(), id)){//check for new name whether it exists on database or not
            return new ResponseEntity("organization name exists",HttpStatus.CONFLICT);
        }
        selected.setPhone(org.getPhone());
        selected.setPhoto(org.getPhoto());
        selected.setType(org.getType());
        oServices.updateOrganization(selected);
        return new ResponseEntity("ok",HttpStatus.OK);
    }
}
