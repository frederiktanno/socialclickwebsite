/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.controllers;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Random;
//import com.mycompany.socialclickwebsite.models.Users;
/**
 *
 * @author ASUS
 */
public class PasswordHash {
    private static final Random RANDOM = new SecureRandom();
    
    public static byte[] getNextSalt() {
        byte[] salt = new byte[16];
        RANDOM.nextBytes(salt);
        return salt;
    }
    
    public String hashPassword(String pass)throws Exception{
        byte[]salt=getNextSalt();
         MessageDigest md = MessageDigest.getInstance("SHA-256");
         md.update(pass.getBytes());
         md.update(salt);
         byte byteData[] = md.digest();
         StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
}
