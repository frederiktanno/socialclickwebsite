/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.controllers;
import com.mycompany.socialclickwebsite.models.Gallery;
import com.mycompany.socialclickwebsite.models.Project;
import com.mycompany.socialclickwebsite.models.ProjectMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.mycompany.socialclickwebsite.services.BookmarkServices;
import com.mycompany.socialclickwebsite.services.OrganizationServices;
import com.mycompany.socialclickwebsite.services.UserServices;
import com.mycompany.socialclickwebsite.services.JWTUniversalValidationServices;
import com.mycompany.socialclickwebsite.services.ProjectServices;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
/**
 *
 * @author ASUS
 */
//@CrossOrigin(origins="*",maxAge=3600)
@CrossOrigin
@RestController
@ComponentScan("com.mycompany.socialclickwebsite.services")
public class ProjectController {
    @Autowired
    private UserServices uServices;
    
    @Autowired
    private OrganizationServices orgServices;
    
    @Autowired
    private JWTUniversalValidationServices jwtServices;
    
    @Autowired
    private ProjectServices pServices;
    
    @RequestMapping(value="/projects/myProject",method=RequestMethod.GET)
    public ResponseEntity <List<Project>>initiateProject(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){
        String validate=jwtServices.validateToken(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        String role=jwtServices.getClaim(token);
        if("organization".equals(role)){
            List<Project> project=pServices.getOrgInitiatedProject(id);
            if(project==null){
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity(project,HttpStatus.OK);
        }
        List<Project>project=pServices.getUserIniatedProject(id);//fetch user's initiated project
        if(project==null){
            return new ResponseEntity("empty",HttpStatus.OK);
        }
        return new ResponseEntity(project,HttpStatus.OK);
    }
    
    @RequestMapping(value="projects/testProject",method=RequestMethod.GET)
    public ResponseEntity<List<Project>>testProject(){
        //DateFormat date=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        //date.setTimeZone(TimeZone.getTimeZone("GMT+7"));
        /*String test=pServices.joinProject("1", "4");
        if(test.contains("error")){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }*/
        List<Project> project=pServices.getProject();
        if(project == null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        /*if(project.getOrganization()!=null){
            project.getOrganization().setEmail(" ");
            project.getOrganization().setPassword(" ");
            project.getOrganization().setPhoto(" ");
        }
        if(project.getUsers()!=null){
            project.getUsers().setPhoto(" ");
            project.getUsers().setEmail(" ");
            project.getUsers().setPassword(" ");
        }*/
        
        
        /*try{
            Date dateconv=date.parse(datetime);
            System.out.println(dateconv);
            long starttimemillis=dateconv.getTime();
            System.out.println(starttimemillis);
            System.out.println(System.currentTimeMillis());
            String crt="1516206920105";
            long currenttime=Long.parseLong(crt);
            Date dte=new Date(currenttime);
            String dateformatted=date.format(dte);
            System.out.println(dateformatted);
        }
        catch(Exception ex){
            System.out.println(ex.toString());
        }*/
        
        return new ResponseEntity(project,HttpStatus.OK);
    }
    
    @RequestMapping(value="/projects/checkProject",method=RequestMethod.GET)
    public ResponseEntity<Void>CheckProject(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){
        
        String validate=jwtServices.validateToken(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        String role=jwtServices.getClaim(token);
        if("organization".equals(role)){
            return new ResponseEntity(HttpStatus.CONFLICT);
        }
        if(pServices.getUserIniatedProject(id)!=null){//if user already start a project
            return new ResponseEntity("empty",HttpStatus.OK);//not permissioned to add another one
        }
        return new ResponseEntity(HttpStatus.OK);
    }
    //for creating a new project
    @RequestMapping(value="/projects/myProject",method=RequestMethod.POST)
    public ResponseEntity<Void>CreateProject(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @RequestBody Project project,
                                      UriComponentsBuilder ucBuilder){
        
        String validate=jwtServices.validateToken(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        String role=jwtServices.getClaim(token);
        if("organization".equals(role)){
            if(pServices.saveOrgProject(project,id).contains("error")){
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity(HttpStatus.OK);
        }
        if(pServices.getUserIniatedProject(id).isEmpty()==false){//if user already start a project
            return new ResponseEntity(HttpStatus.LOCKED);//not permissioned to add another one
        }
        if(pServices.saveUserProject(project,id).contains("error")){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);       
    }
    
     @RequestMapping(value="/projects/myProject",method=RequestMethod.PUT)
    public ResponseEntity<Void>UpdateProject(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @RequestBody Project project){       
        String validate=jwtServices.validateToken(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Project selected=pServices.getProjectById(project.getId()+"");
        if(selected==null){
                System.out.println("Project not found");
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        selected.setCategory(project.getCategory());
        selected.setDatetime(project.getDatetime());
        selected.setDescription(project.getDescription());
        selected.setGallery(project.getGallery());
        selected.setLocation(project.getLocation());
        selected.setMaxmembers(project.getMaxmembers());
        selected.setName(project.getName());
        //selected.setProjMember(project.getProjMember());
        selected.setRegdeadline(project.getRegdeadline());
        selected.setProfile(project.getProfile());
        selected.setFinished(project.getFinished());
        selected.setHours(project.getHours());
        
        String update=pServices.updateProject(selected);
        if(update.contains("error")){
            return new ResponseEntity("error updating new data",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value="/projects",method=RequestMethod.GET)
    public ResponseEntity<List<Project>>projectList(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){
        String valid=jwtServices.validateToken(token, id);
        if(!"valid".equals(valid)){
            if("token expired".equals(valid)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(valid.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<Project>getProjectList=pServices.getProject();
        if(getProjectList.isEmpty()==true){
            return new ResponseEntity("empty",HttpStatus.OK);
        }
        return new ResponseEntity(getProjectList,HttpStatus.OK);
    }
    
    @RequestMapping(value="projects/{idProject}",method=RequestMethod.GET)
    public ResponseEntity<Project>project(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @PathVariable("idProject")String idProject){
        String valid=jwtServices.validateToken(token, id);
        if(!"valid".equals(valid)){
            if("token expired".equals(valid)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(valid.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Project project=pServices.getProjectById(idProject);
        if(project==null){
            return new ResponseEntity("empty",HttpStatus.NOT_FOUND);
        }
        if(project.getOrganization()!=null){
            project.getOrganization().setEmail(" ");
            project.getOrganization().setPhoto(" ");
        }
        if(project.getUsers()!=null){
            project.getUsers().setPhoto(" ");
            project.getUsers().setEmail(" ");
        }
        return new ResponseEntity(project,HttpStatus.OK);
    }
    
    @RequestMapping(value="/projects/{id}",method=RequestMethod.POST)
    public ResponseEntity<Void>HomeUserPost(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @PathVariable("id") String idProject,
                                      UriComponentsBuilder ucBuilder){
        String validate=uServices.validateUser(token, id);//validate users' token only
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Project project=pServices.getProjectById(idProject);
        if(project==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        int membercount=pServices.countProjectMembers(idProject);
        if(membercount+1>project.getMaxmembers()){
            return new ResponseEntity("max members reached",HttpStatus.BAD_REQUEST);
        }
        String joined=pServices.joinProject(id, idProject);
        if(joined.equals("error: you have joined some projects")){
            return new ResponseEntity("already joined a project",HttpStatus.NOT_ACCEPTABLE);
        }
        else if(joined.equals("error: you cannot register to this project")){
            return new ResponseEntity("registration deadline already passed",HttpStatus.LOCKED);
        }
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value="/projects/joinedProjects",method=RequestMethod.GET)
    public ResponseEntity<List<Project>>getJoinedProject(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){
        String validate=uServices.validateUser(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        System.out.println(id);
        List<Project>joinedProjects=pServices.getJoinedProject(id);
        if(joinedProjects==null){
            return new ResponseEntity("error",HttpStatus.NO_CONTENT);
        }
        else if(joinedProjects.isEmpty()==true){
            return new ResponseEntity("empty",HttpStatus.OK);
        }
        return new ResponseEntity(joinedProjects,HttpStatus.OK);
    }
    
    @RequestMapping(value="/projectTest/{id}",method=RequestMethod.GET)
    public ResponseEntity<Void>testProject(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @PathVariable("id") String idProject){
         String validate=uServices.validateUser(token, id);//validate users' token only
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        int membercount=pServices.countProjectMembers(idProject);
        System.out.println(membercount);
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value="projects/{id}/gallery",method=RequestMethod.GET)
    public ResponseEntity<List<Gallery>>getGallery(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @PathVariable("id") String idProject){
        String valid=jwtServices.validateToken(token, id);
        if(!"valid".equals(valid)){
            if("token expired".equals(valid)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(valid.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<Gallery>galleries=pServices.getProjectGallery(idProject);
        if(galleries.isEmpty()==true){
            return new ResponseEntity("empty",HttpStatus.OK);
        }
        return new ResponseEntity(galleries,HttpStatus.OK);
    }
    
    @RequestMapping(value="projects/getbycategory",method=RequestMethod.GET)
    public ResponseEntity<List<Project>>getByCategory(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @RequestHeader(value="idCategory") String[]idCategory){
        System.out.println(idCategory[0]);
        System.out.println(idCategory[1]);
        String valid=jwtServices.validateToken(token, id);
        if(!"valid".equals(valid)){
            if("token expired".equals(valid)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(valid.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        //System.out.println(idCategory[1]);
        //Project projects=pServices.getProjectById("10");
        //List<ProjectMember>projMem=pServices.getProjectMembers("4");
        //System.out.println(projMem);
        //List<Project>projects=pServices.getProject();
        List<Project>projects=pServices.getProjectsByCategory(idCategory);
        if(projects==null){
            return new ResponseEntity("error",HttpStatus.BAD_REQUEST);
        }
        if(projects.isEmpty()==true){
            return new ResponseEntity("empty",HttpStatus.OK);
        }
        return new ResponseEntity(projects,HttpStatus.OK);
    }
    
    @RequestMapping(value="projects/{id}/finishProject",method=RequestMethod.PUT)
    public ResponseEntity<String>finishProject(@PathVariable(value="id")String idProject,
                                                @RequestBody Project project){
        String finish=pServices.finishProject(idProject,project.getHours());
        if(finish.contains("error")){
            return new ResponseEntity("error updating data",HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }
    
}
