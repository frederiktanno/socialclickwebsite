/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Date;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
/**
 *
 * @author ASUS
 */
@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent>{
    
    @Autowired
    private JavaMailSenderImpl msender;
    
    @Override
    public void onApplicationEvent( OnRegistrationCompleteEvent event) {
        this.confirmRegistration(event);
    }
    private String confirmRegistration(OnRegistrationCompleteEvent event){
            try{//generate token here
                long time=System.currentTimeMillis();
                String secret="umsebenzisi";
                String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
                System.out.println(encoded);
                Algorithm algorithm = Algorithm.HMAC256(encoded);
                Date now=new Date(time);
                Date end=new Date(time+ 86400000);//1 day expiry time
                System.out.println(now);
                System.out.println(end);
                String emailtoken=event.getUser().getEmail();
                String token = JWT.create()
                    .withIssuer("http://socialclick.web.id").withIssuedAt(now).withExpiresAt(end)
                    .withClaim("access","user").withClaim("admin", Boolean.FALSE).withClaim("id", event.getUser().getId())
                    .withClaim("verified", Boolean.FALSE)
                    .sign(algorithm);
                    //create JWT token with issuer , and claims of user's id in database
                    //and access type of 'user'
                String mail=this.sendEmail(token,emailtoken,event.getUser().getId());
                if(!"sent".equals(mail)){
                    return "error";
                }
                return token;
            }
            catch (UnsupportedEncodingException exception){
                System.out.println(exception.toString());
                return exception.toString()+"error";
                //UTF-8 encoding not supported
            } 
            catch (JWTCreationException exception){
                //Invalid Signing configuration / Couldn't convert Claims.
                System.out.println(exception.toString());
                return exception.toString()+"error";
            }
            catch(MailException ex) {
                // simply log it and go on...
                System.err.println(ex.getMessage());
                return ex.toString()+"error";
            }
            catch(Exception ex){
                System.err.println(ex.getMessage());
                return ex.toString()+"error";
            }
    
            
    }
    
    public String sendEmail(String token,String email,long id){
        MimeMessage mime=msender.createMimeMessage();
        MimeMessageHelper msg = new MimeMessageHelper(mime);
        //sending an email with token as path variable
        try{
            msg.setFrom("frederiktanno@gmail.com");
            msg.setTo(email);
            msg.setText(
                "Click on this link to verify your email http://socialclick.web.id/emailValidation/"+token
                    +"/"+id);
            msg.setSubject("Please Verify Your Email-SocialClick");
            msender.send(mime);
            return "sent";
        }
        catch(MailException ex) {
            // simply log it and go on...
            System.err.println(ex.getMessage());
            return ex.toString()+"error";
        } 
        catch (MessagingException ex) {
            System.err.println(ex.getMessage());
            return ex.toString()+"error";
        }
        
    }
    
}
