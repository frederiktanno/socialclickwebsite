/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.controllers;

import com.mycompany.socialclickwebsite.models.Bookmark;
import com.mycompany.socialclickwebsite.models.Organization;
import com.mycompany.socialclickwebsite.models.Project;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.mycompany.socialclickwebsite.models.Users;
import com.mycompany.socialclickwebsite.services.BookmarkServices;
import com.mycompany.socialclickwebsite.services.JWTUniversalValidationServices;
import com.mycompany.socialclickwebsite.services.OrganizationServices;
import com.mycompany.socialclickwebsite.services.ProjectServices;
import com.mycompany.socialclickwebsite.services.UserServices;
import java.util.Arrays;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 *
 * @author ASUS
 */
//@CrossOrigin(origins="*",maxAge=3600)
@CrossOrigin
@RestController
@ComponentScan("com.mycompany.socialclickwebsite.services")
public class UserController {
    //i'm not using the jwtUniversalValidation because i want to prevent users with different access levels
    //to access routes where they should not be in
    @Autowired
    private UserServices uServices;
    
    @Autowired
    private ProjectServices pServices;
    
    @Autowired
    private JWTUniversalValidationServices jwtServices;
    
    @Autowired
    private BookmarkServices bServices;
    
    @RequestMapping(value="/user/myProfile/{id}",method=RequestMethod.GET)
    public ResponseEntity<Users>getUser(
                                        @RequestHeader(value="token")String token,
                                        @RequestHeader(value="id")String id,
                                        @PathVariable("id") String idUser){
        // ERIK JGN DIGANTI LGI PLEASE.
        String validate=uServices.validateUser(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Users user=uServices.findById(idUser);
        if(user==null){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        System.out.println(id);
        //user.setPassword("");
        int hours=pServices.countTotalHours(id);
        user.setHours(hours);
        //System.out.println(validate);
        System.out.println(user);
        return new ResponseEntity(user,HttpStatus.OK);
    }
    
    @RequestMapping(value="/user/myProfile",method=RequestMethod.PUT)
    public ResponseEntity<Void>updateUser(@RequestBody Users user,
                                      @RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){
         String validate=uServices.validateUser(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Users selected=uServices.findById(id);
        if(selected==null){
                System.out.println("User not found");
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        selected.setAddress(user.getAddress());
        selected.setAddress2(user.getAddress2());
        selected.setAddress3(user.getAddress3());
        selected.setBirthplace(user.getBirthplace());
        selected.setDOB(user.getDOB());
        selected.setDescription(user.getDescription());
        
        selected.setExperience(user.getExperience());
        selected.setGender(user.getGender());
        selected.setIdnum(user.getIdnum());
        selected.setInterest(user.getInterest());
        selected.setLastedu(user.getLastedu());
        selected.setNationality(user.getNationality());
        selected.setOccupation(user.getOccupation());
        selected.setPhone(user.getPhone());
        selected.setPostcode(user.getPostcode());
        selected.setStateName(user.getStateName());
        selected.setfname(user.getfname());
        selected.setlname(user.getlname());
        selected.setPhoto(user.getPhoto());
        selected.setEmail(user.getEmail());
        if(uServices.checkEmail(user.getEmail(), id)){//check for new email whether it exists on database or not
            return new ResponseEntity("email exists",HttpStatus.CONFLICT);
        }
        uServices.updateUser(selected);
        return new ResponseEntity(HttpStatus.OK);
    }
    
    @RequestMapping(value="/user/myProfile/",method=RequestMethod.DELETE)
    public ResponseEntity<Void>deleteUser(
                                      @RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){
        
         String validate=uServices.validateUser(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Users selected=uServices.findById(id);
        if(selected==null){
                System.out.println("User not found");
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }
        uServices.deleteUserById(id);
        return new ResponseEntity(HttpStatus.OK);
    }
    
    //return bolean aj, jdi dia check project yg di access ama user ini klo ad true,
    //klo ga ad return false
    // for erik, tolong check BookmarkDaoImpl.getBookmark() dong, bnr ato ga wakkwakwak.
    //bookmark cmn perlu ListBookmark Method Get, checkBookmark method GET ama enterBookmark Method POST.
    //bookmark perlu DeleteBookmark Method Delete.
    @RequestMapping(value="/bookmark/{id}", method =RequestMethod.GET)
    public ResponseEntity<Boolean> checkBookmark(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      @PathVariable("id") String idProject){
        String validate=uServices.validateUser(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        Boolean check = bServices.getBookmark(id, idProject);
        if(check==true){
            return new ResponseEntity(check,HttpStatus.NOT_ACCEPTABLE);
        }
        return new ResponseEntity(check , HttpStatus.OK);
        
    }
    
    @RequestMapping(value="/bookmark", method =RequestMethod.GET)
    public ResponseEntity<List<Project>> getBookmark(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id){
        String validate=uServices.validateUser(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<Project> book=bServices.getBookmarkById(id);
        if(book==null){
            return new ResponseEntity("error",HttpStatus.BAD_REQUEST);
        }
        if(book.isEmpty()==true){
            return new ResponseEntity("empty",HttpStatus.OK);
        }
        return new ResponseEntity(book, HttpStatus.OK);
        
    }
    
    @RequestMapping(value="/bookmark", method=RequestMethod.POST)
    public ResponseEntity<Void> enterBookmark(@RequestHeader(value="token")String token,
                                      @RequestHeader(value="id")String id,
                                      
                                      @RequestBody Bookmark bookmark,
                                      UriComponentsBuilder ucBuilder){
        String validate=uServices.validateUser(token, id);
        if(!"valid".equals(validate)){
            if("token expired".equals(validate)){
                return new ResponseEntity(HttpStatus.FORBIDDEN);
            }
            else if(validate.equals("blocked")){
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
          return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        //bookmark.setUser(user);
        String post;
        post=bServices.saveBookmark(bookmark);
        if("error".equals(post)){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }
}
