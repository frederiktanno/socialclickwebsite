/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author EdbertX
 */
public abstract class AbstractDao {
    @Autowired
    
    public SessionFactory sessionFactory;
    
    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }
    
    public void persist(Object entity){
        getSession().save(entity);
       
    }
    
    public void deletet(Object entity){
        getSession().delete(entity);
    }
      
}