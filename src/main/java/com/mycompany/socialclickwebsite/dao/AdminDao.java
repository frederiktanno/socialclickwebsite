/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;
import java.util.List;
import com.mycompany.socialclickwebsite.models.Project;
import com.mycompany.socialclickwebsite.models.Organization;
import com.mycompany.socialclickwebsite.models.Users;
/**
 *
 * @author ASUS
 */
public interface AdminDao {
    String validateOrg(String id);
    String banUser(String id);
    String regisAdmin(Users user);
    String loginAdmin(String email,String password);
}
