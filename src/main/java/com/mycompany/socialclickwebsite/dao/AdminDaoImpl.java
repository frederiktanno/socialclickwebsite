/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import javax.persistence.EntityNotFoundException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.auth0.jwt.interfaces.Claim;
import com.mycompany.socialclickwebsite.models.Organization;
import com.mycompany.socialclickwebsite.models.Users;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
/**
 *
 * @author ASUS
 */
@Repository("AdminDao")
public class AdminDaoImpl extends AbstractDao implements AdminDao {
    
    Date now;
    Date end;
    DateFormat date=new SimpleDateFormat("dd/MM/yyyy");
    
    @Override
    @Transactional
    public String validateOrg(String id){
        try{
          Query q=getSession().createSQLQuery("UPDATE `organization` set `verified`=:verified where `id`=:id");
            q.setParameter("id", id);
            q.executeUpdate();
            return "ok";
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            return ex+" error";
        }
        
    }
    
    @Override
    @Transactional
    public String regisAdmin(Users user){
        try{
            Query qUser;
            System.out.println(user.getPassword());
            qUser=getSession().createSQLQuery("SELECT `email` from `user` WHERE `email`=:email");
            qUser.setString("email",user.getEmail());
            Object mail=qUser.uniqueResult();            
            if(mail!=null){
                return "email exist";
            }
            String emailverif=user.getEmail();
            String token;
            Query q;
            q=getSession().createSQLQuery("SELECT `id` from `user` WHERE `email`=:email");
            String id=q.list().get(0).toString();
            user.setAccess(1);//set permission to admin
           long time=System.currentTimeMillis();
            String secret="umnikazi";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println(encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);
            now=new Date(time);
            end=new Date(time+ 86400000);//1 day expiry time
            System.out.println(now);
            System.out.println(end);
            token = JWT.create()
                .withIssuer("http://socialclick.web.id").withIssuedAt(now).withExpiresAt(end)
                .withClaim("id", id).withClaim("access","admin").withClaim("admin", Boolean.TRUE)
                .sign(algorithm);//create JWT token with issuer auth0, and claims of user's id in database
                //and access type of 'user'
            /*q=getSession().createSQLQuery("INSERT into `token`(`idUser`,`token`,`validTime`)VALUES (:id,:token,:time)");
            q.setString("id", id);
            q.setString("token", token);
            q.setString("time", (time+86400000) / 1000 + "");
            q.executeUpdate();*/
            System.out.println(token);
            return token;
        }catch (UnsupportedEncodingException exception){
            return exception.toString()+"error";
            //UTF-8 encoding not supported
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
            return exception.toString()+"error";
        } 
    }
    
    @Override
    public String loginAdmin(String email,String password){
        try {
            Query qEmail;
            qEmail=getSession().createSQLQuery("SELECT `email` from `user` where `email`=:email");
            qEmail.setString("email", email);
            Object mail=qEmail.uniqueResult();           
            if(mail==null){
                return "invalid email or password";
            }
            String emailverif=mail.toString();
            Query qUser,qPass;
            qPass=getSession().createSQLQuery("SELECT `password` from `user` where `email`=:email");
            qPass.setString("email",email);
            List passList=qPass.list();
            String pass=(String) passList.get(0);
            //boolean match=passwordEncoder.matches(pass, user.getPassword());
            if(!pass.equals(password)){
                return "invalid email or password";
            }
            //String password=user.getPassword();
            //System.out.println(user.getPassword());
            qUser=getSession().createSQLQuery("SELECT `id` from `user` WHERE `email`=:email AND `password`=:password"
                    + "AND `accessLevel`=:accesslevel");
            qUser.setString("email",email);
            qUser.setString("password", password);
            qUser.setString("accesslevel","1");
            List list=qUser.list();//gets user id with email and password credential
            if(list.isEmpty()){
                return "admin not found";
            }
            String id=list.get(0).toString();
            String token;
            /*Query test;
            test=getSession().createSQLQuery("SELECT `idUser` from `token` WHERE `idUser`=:id");
            test.setString("id",id);
            Object iduser=test.uniqueResult();//checks for duplicate logged in user
            if(iduser!=null){
                System.out.println("userexist");
                return "userexist";
            }*/            
            long time=System.currentTimeMillis();
            String secret="umnikazi";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println(encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);   
            now=new Date(time);
            end=new Date(time+ 86400000);//1 day expiry time
            System.out.println(now);
            System.out.println(end);
            token = JWT.create()
                .withIssuer("http://socialclick.web.id").withIssuedAt(now).withExpiresAt(end)
                .withClaim("id", id).withClaim("access","admin").withClaim("admin", Boolean.TRUE)
                .sign(algorithm);//create JWT token with issuer auth0, and claims of user's id in database
                //and access type of 'user'
             //claims.put("id",idtoken);
             //claims.put("access",access);
           /* q=getSession().createSQLQuery("INSERT into `token`(`idUser`,`token`,`validTime`)VALUES (:id,:token,:time)");
            q.setString("id", id);
            q.setString("token", token);
            //q.setString("time", (time + 3600000) / 1000 + "");
            q.setString("time",(time+86400000)/1000+"");
            q.executeUpdate();*/
            System.out.println(token);
            return token;
        } catch (UnsupportedEncodingException exception){
            return exception.toString();
            //UTF-8 encoding not supported
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
            System.out.println(exception.toString());
            return exception.toString()+"error";
        } 
        catch(EntityNotFoundException ex){
            System.out.println(ex.toString());
            return ex.toString()+"error";
        }
        catch(NullPointerException exception){
            System.out.println(exception.toString());
            return exception.toString();
        }
    }
    
    @Override
    public String banUser(String id){
        Query q;
        try{
            q=getSession().createSQLQuery("UPDATE `user` set `status`=:status where `id`=:id");
            q.setParameter("status", 2);
            q.setParameter("id",id);
            q.executeUpdate();
            return "ok";
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            return ex.toString()+"error";
        }
    }
    
}
