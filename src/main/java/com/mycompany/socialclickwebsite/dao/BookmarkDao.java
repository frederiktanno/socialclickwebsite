/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;

import com.mycompany.socialclickwebsite.models.Bookmark;
import java.util.List;
import com.mycompany.socialclickwebsite.models.Project;

/**
 *
 * @author Jason Kusuma
 */
public interface BookmarkDao {
    List<Project> getBookmarkById(String id);
    //List<Bookmark> getBookmark();
    String saveBookmark(Bookmark bookmark);
    void updateBookmark(Bookmark bm);
    void deleteBookmarkById(long id);
    Boolean getBookmark(String idUser, String idProject);
}
