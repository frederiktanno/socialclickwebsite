/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;
import com.mycompany.socialclickwebsite.models.Bookmark;
import com.mycompany.socialclickwebsite.models.Project;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import com.mycompany.socialclickwebsite.models.Gallery;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author Jason Kusuma
 */

@Repository("BookmarkDao")
public class BookmarkDaoImpl extends AbstractDao implements BookmarkDao {
    @Override
    public List<Project> getBookmarkById(String id){
        
        List<Project> list = new ArrayList<>();
        Query q = getSession().createSQLQuery("SELECT `idBookmark` from `bookmark` where `idUser`=:idUser");
        q.setParameter("idUser", id);
        BigInteger idBookmark=(BigInteger) q.uniqueResult();
        if(idBookmark==null){
            return list;
        }
        System.out.println(idBookmark);
        q=getSession().createSQLQuery("SELECT `idProject` from `bookmarkitems` where `idBookmark`=:idBookmark");
        q.setParameter("idBookmark",idBookmark);
        List<BigInteger> idProject = q.list();
        System.out.println(idProject);       
        if(idProject.size() < 1)
            return list;
        int i = 0;       
        try{
           while(i<idProject.size()){
                String idproject=idProject.get(i).toString();           
                q=getSession().createSQLQuery("SELECT * from `project` where `id`=:id");
                q.setParameter("id", idproject);
                //q.setParameter("finished",0);
                List<Object> result = q.list(); 
                    Iterator itr = result.iterator();
                    while(itr.hasNext()){
                       Object[] obj = (Object[]) itr.next();
                       Project project = new Project();
                       project.setId(Long.parseLong(String.valueOf(obj[0])));
                       project.setName(String.valueOf(obj[1]));
                       project.setDescription(String.valueOf(obj[2]));
                       project.setDatetime(String.valueOf(obj[3]));
                       project.setLocation(String.valueOf(obj[4]));
                       project.setRegdeadline(String.valueOf(obj[5]));
                       project.setMaxmembers(Integer.parseInt(String.valueOf(obj[6])));
                       project.setRating(Float.parseFloat(String.valueOf(obj[7])));
                       project.setFinished(Integer.parseInt(String.valueOf(obj[9])));
                       
                       list.add(project);
                    }
                i++;
            }
           return list;
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;    
        }
        catch (Exception ex){
            System.out.println(ex);
            return null;
        }
    }
    
    @Override
    @Transactional
    public String saveBookmark(Bookmark bookmark){
        long iduser=bookmark.getUser().getId();
        System.out.println(iduser);
        long idproject=bookmark.getProjects().get(0).getId();
        System.out.println(idproject);
        Query q;
        try{
            q=getSession().createSQLQuery("SELECT `idBookmark` FROM `bookmark` where `idUser`=:idUser");
            q.setParameter("idUser", iduser);
            List<BigInteger>idbookmark=q.list();
            System.out.println(idbookmark);
            
            if(idbookmark.isEmpty()==true){
               System.out.println("empty");
               q=getSession().createSQLQuery("INSERT INTO `bookmark`(`idBookmark`,`idUser`)VALUES(NULL,:idUser)");
               q.setParameter("idUser", iduser);
               q.executeUpdate();
               System.out.println("inserted into bookmark");
               q=getSession().createSQLQuery("INSERT INTO `bookmarkitems` (`idProject`,`idBookmark`)VALUES(:idProject,"
                       + "(SELECT `idBookmark` FROM `bookmark` where `idUser`=:idUser))");
               q.setParameter("idProject",idproject);
               q.setParameter("idUser",iduser);
               q.executeUpdate();
               System.out.println("inserted into bookmarkitems");
               return "ok";
            }
            
            System.out.println("notempty");
            q=getSession().createSQLQuery("INSERT INTO `bookmarkitems` (`idProject`,`idBookmark`)VALUES(:idProject,"
                       + "(SELECT `idBookmark` FROM `bookmark` where `idUser`=:idUser))");
            q.setParameter("idProject",idproject);
            q.setParameter("idUser",iduser);
            q.executeUpdate();
            return "ok";
        }
        catch(HibernateException ex){
            System.out.println(ex.toString());
            return "error";
        }
    }
    
    @Override
    public Boolean getBookmark(String idUser, String idProject){
        Query q=getSession().createSQLQuery("SELECT `idProject`FROM `bookmarkitems`WHERE `idProject`=:idProject AND "
                + "`idBookmark`IN(SELECT `idBookmark` FROM `bookmark`WHERE `idUser`=:idUser)");
        q.setString("idProject",idProject);
        q.setString("idUser", idUser);
        List<BigInteger> idProjects = q.list();
        if(idProjects.size()>0){
            return true;
        }
        /*q = getSession().createSQLQuery("SELECT `idBookmark` from `bookmark` where `idUser`=:idUser");
        q.setString("idUser", idUser);
        List<BigInteger> idBookmarks = q.list();
        System.out.println(idBookmarks);
        if(idBookmarks.size() < 1)
            return false;
        int i = 0;
        List<Long> projectList = new ArrayList<>();
        try{
            BigInteger idBookmark = idBookmarks.get(i);
            System.out.println(idBookmark);
            q = getSession().createSQLQuery("SELECT `idProject` from `bookmarkitems` where `idBookmark`=:idBookmark");
            q.setParameter("idBookmark", idBookmark);
            System.out.println(q.list());
            List<Object> result = q.list();
            Iterator itr = result.iterator();
            while(itr.hasNext()){
                Object[] obj = (Object[]) itr.next();
                long project = Long.parseLong(String.valueOf(obj[0]));
                projectList.add(project);
            }
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;    
        }
        int j = 0;
        while(j < projectList.size()){
            if(projectList.get(j) == Long.parseLong(idProject)){
                return true;
            }
        }*/
        return false;
    }
    
//    @Override
//    @SuppressWarnings("unchecked")
//    public List<Bookmark>getBookmark(){
//     Criteria criteria=getSession().createCriteria(Bookmark.class);
//        return (List<Bookmark>)criteria.list();
//    }
    
    @Override
    @Transactional
    public void updateBookmark(Bookmark bm){
        getSession().update(bm);
    }
    
    @Override
    @Transactional
    public void deleteBookmarkById(long id){
        Query q = getSession().createSQLQuery("DELETE FROM `bookmark` WHERE `idUser` = :id");
        q.executeUpdate();
    }
}
