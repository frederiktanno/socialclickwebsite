/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;
import com.mycompany.socialclickwebsite.models.Category;
import java.util.List;
/**
 *
 * @author ASUS
 */
public interface CategoryDao {
    Category getCategoryById(int id);
    List<Category> getCategory();
    Category getCategoryByName(String name);
    void saveCategory(Category interest);
}
