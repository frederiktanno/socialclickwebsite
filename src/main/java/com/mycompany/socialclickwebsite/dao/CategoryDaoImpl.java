/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;
import com.mycompany.socialclickwebsite.models.Category;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Repository("CategoryDao")
public class CategoryDaoImpl extends AbstractDao implements CategoryDao{
    @Override
    public Category getCategoryById(int id){
        try{
            Criteria c = getSession().createCriteria(Category.class);
            c.add(Restrictions.eq("id", id));
            return (Category) c.uniqueResult();
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
    }
    @Override
    public Category getCategoryByName(String name){
        Criteria criteria=getSession().createCriteria(Category.class);
        criteria.add(Restrictions.eq("category", name));
        return (Category)criteria.uniqueResult();
    }
    
    @Override
    @Transactional
    public void saveCategory(Category interest){
        persist(interest);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Category>getCategory(){
        try{
           Criteria criteria=getSession().createCriteria(Category.class);
            return (List<Category>)criteria.list(); 
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
    }
}
