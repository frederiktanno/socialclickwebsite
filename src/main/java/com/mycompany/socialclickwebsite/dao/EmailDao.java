/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;

/**
 *
 * @author ASUS
 */
public interface EmailDao {
    String sendEmail(String token,String email);
}
