/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;


/**
 *
 * @author ASUS
 */
@Repository("EmailDao")
public class EmailDaoImpl extends AbstractDao implements EmailDao{
    
    @Autowired
    private JavaMailSenderImpl msender;
    
    /*
        sending email using the java and Spring mail libraries
        
    */
    @Override
    public String sendEmail(String token,String email){
        MimeMessage mime=msender.createMimeMessage();
        MimeMessageHelper msg = new MimeMessageHelper(mime);
        
        try{
            msg.setFrom("frederiktanno@gmail.com");
            msg.setTo("l.frederik.tanno@gmail.com");
            msg.setText(
                "test");
            msg.setSubject("Hello there");
            msender.send(mime);
            return "sent";
        }
        catch(MailException ex) {
            // simply log it and go on...
            System.err.println(ex.getMessage());
            return ex.toString()+"error";
        } 
        catch (MessagingException ex) {
            System.err.println(ex.getMessage());
            return ex.toString()+"error";
        }
        
    }
}
