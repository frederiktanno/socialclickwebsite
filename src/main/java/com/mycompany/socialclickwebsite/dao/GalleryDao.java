/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;
import com.mycompany.socialclickwebsite.models.Gallery;
import java.util.List;
/**
 *
 * @author ASUS
 */
public interface GalleryDao {
    List<Gallery>getGallery();
    void saveGallery(Gallery gallery);
}
