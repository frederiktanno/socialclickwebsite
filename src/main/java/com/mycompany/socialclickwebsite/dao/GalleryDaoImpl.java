/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;
import com.mycompany.socialclickwebsite.models.Gallery;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Repository("GalleryDao")
public class GalleryDaoImpl extends AbstractDao implements GalleryDao{
    
    @Override
    @Transactional
    public void saveGallery(Gallery gallery){
        persist(gallery);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Gallery>getGallery(){
        try{
            Criteria criteria=getSession().createCriteria(Gallery.class);
            return (List<Gallery>)criteria.list();
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
    }
}
