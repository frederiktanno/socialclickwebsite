/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;
import com.mycompany.socialclickwebsite.models.Interest;
import java.util.List;
/**
 *
 * @author ASUS
 */
public interface InterestDao {
    Interest getInterestById(int id);
    List<Interest> getInterest();
    Interest getInterestByName(String name);
    void saveInterest(Interest interest);
}
