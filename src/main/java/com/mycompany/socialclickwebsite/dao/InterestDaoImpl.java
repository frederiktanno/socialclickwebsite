/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;

import com.mycompany.socialclickwebsite.models.Interest;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marvint11
 */
@Repository("InterestDao")
public class InterestDaoImpl extends AbstractDao implements InterestDao{
    
    
    @Override
    public Interest getInterestById(int id){
        try{
            Criteria c = getSession().createCriteria(Interest.class);
            c.add(Restrictions.eq("id", id));
            return (Interest) c.uniqueResult();
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
    }
    @Override
    public Interest getInterestByName(String name){
        Criteria criteria=getSession().createCriteria(Interest.class);
        criteria.add(Restrictions.eq("interest", name));
        return (Interest)criteria.uniqueResult();
    }
    
    @Override
    @Transactional
    public void saveInterest(Interest interest){
        persist(interest);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Interest>getInterest(){
        try{
            Criteria criteria=getSession().createCriteria(Interest.class);
            return (List<Interest>)criteria.list();
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
    }
}
