/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;

/**
 *
 * @author ASUS
 */
public interface JWTUniversalValidationDao {
    String validateToken(String token,String id);
    String validateOrg(String token,String id);
    String validateAdmin(String token,String id);
    String getClaim(String token);
}
