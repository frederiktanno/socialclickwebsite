/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Repository("JWTUniversalValidationDao")
public class JWTUniversalValidationDaoImpl extends AbstractDao implements JWTUniversalValidationDao{
    //validate token using user credential first
    //if fails, then use organization credentials
    //if either fails, then token is rejected
    
    DecodedJWT jwt;
    @Override
    public String validateToken(String token,String id){
        
        try {
            String secret="umsebenzisi";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println("valid "+encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);
            JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("http://socialclick.web.id")
                .withClaim("access","user").withClaim("id",id).withClaim("admin", Boolean.FALSE)    
                .build(); //Reusable verifier instance
            jwt=JWT.decode(token);
            Map<String, Claim> claims = jwt.getClaims();    //Key is the Claim name
            String claim = claims.get("access").asString();
            String idUser=claims.get("id").asString();
            System.out.println("access "+claim);
            boolean admin=jwt.getClaim("admin").asBoolean();
            System.out.println("admin "+admin);
            if("organization".equals(claim)){
                String valid=validateOrg(token,id);
                return valid;
            }
            else if(admin==true){
                String valid=validateAdmin(token,id);
                return valid;
            }
            //check if user is blocked or not
            Query q=getSession ().createSQLQuery("SELECT `status` from `user` where `id`=:id");
            q.setParameter("id",idUser);
           int status= (int) q.list().get(0);
            if(status==2){
                return "blocked";
            }
            System.out.println("user");
            jwt = verifier.verify(token);          
            return "valid";
        } 
        catch (UnsupportedEncodingException exception){
            //UTF-8 encoding not supported
            System.out.println(exception.toString());
            return exception.toString();
        } 
        catch (InvalidClaimException exception){
            //Invalid signature/claims
            /*String org=validateOrg(token,id);//validate using organization's credentials
            if(org!="valid"){
               /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);     
                return "invalid claim";
            }
            return org;*/
            return exception.toString();
        }
        catch (TokenExpiredException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "token expired";
        }
        catch(AlgorithmMismatchException ex){
           return ex.toString();
        }
        catch(JWTDecodeException ex){
            return ex.toString();
        }
        catch(SignatureVerificationException ex){
            return ex.toString();       
        }
    }
    
    @Override
    public String validateOrg(String token,String id){
        try {
            String secret="inhlangano";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println("valid "+encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);
            JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("http://socialclick.web.id")
                .withClaim("access","organization").withClaim("id",id).withClaim("admin", Boolean.FALSE)    
                .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);
            Map<String, Claim> claims = jwt.getClaims();    //Key is the Claim name
            String claim = claims.get("access").toString();
            return "valid";
        } 
        catch (UnsupportedEncodingException exception){
            //UTF-8 encoding not supported
            System.out.println(exception.toString());
            return exception.toString();
        } 
        catch (InvalidClaimException exception){
            //Invalid signature/claims           
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "invalid claim";
        }
        catch (TokenExpiredException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "token expired";
        }
        catch(AlgorithmMismatchException ex){
           /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "invalid algorithm used";
        }
        catch(JWTDecodeException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error decoding token";
        }
        catch(SignatureVerificationException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error verifying signature";
        }        
    }
    
    @Override
     public String validateAdmin(String token,String id){
        try {
            String secret="umnikazi";//Zulu for owner
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println("valid "+encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);
            JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("http://socialclick.web.id")
                .withClaim("access","admin").withClaim("id",id).withClaim("admin", Boolean.TRUE)    
                .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);
            Map<String, Claim> claims = jwt.getClaims();    //Key is the Claim name
            String claim = claims.get("access").toString();
            return "valid";
        } 
        catch (UnsupportedEncodingException exception){
            //UTF-8 encoding not supported
            System.out.println(exception.toString());
            return exception.toString();
        } 
        catch (InvalidClaimException exception){
            //Invalid signature/claims           
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "invalid claim";
        }
        catch (TokenExpiredException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "token expired";
        }
        catch(AlgorithmMismatchException ex){
           /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "invalid algorithm used";
        }
        catch(JWTDecodeException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error decoding token";
        }
        catch(SignatureVerificationException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error verifying signature";
        }        
    }
     
     @Override
     public String getClaim(String token){
         DecodedJWT jwtClaim;
         jwtClaim=JWT.decode(token);
         String role=jwtClaim.getClaim("access").asString();
         return role;        
     }
}
