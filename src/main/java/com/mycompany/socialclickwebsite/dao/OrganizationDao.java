/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;
import com.mycompany.socialclickwebsite.models.Organization;
import com.mycompany.socialclickwebsite.models.Token;
import java.util.List;
/**
 *
 * @author ASUS
 */
public interface OrganizationDao {
    void saveOrganization(Organization organization);
    List<Organization>getOrganization();
    Organization getOrganizationById(long id);
    Organization getOrganizationByName(String name);
    Organization getOrganizationByEmail(String email);
    boolean checkOrgEmail(String email,String id);
    boolean checkOrgName(String name, String id);
    void updateOrganization(Organization organization);
    void deleteOrganizationById(String id);
    void setVerification(String id,String verification);
    void setStatus(String id,String status);
    String loginAuth(String email, String password);
    String regisAuth(Organization org);
    String getToken(String token);
    String validateOrg(String token,String id);
    String returnId(String email);
}
