/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.dao;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.InvalidClaimException;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.mycompany.socialclickwebsite.models.Organization;
import com.mycompany.socialclickwebsite.models.Token;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityNotFoundException;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marvint11
 */
@Repository("OrganizationDao")
public class OrganizationDaoImpl extends AbstractDao implements OrganizationDao{
    
    @Override
    @Transactional
    public void saveOrganization(Organization organization){
        persist(organization);
    }
    @Override
    @SuppressWarnings("unchecked")
    public List<Organization>getOrganization(){
        try{
           Criteria c = getSession().createCriteria(Organization.class);
            return (List<Organization>) c.list(); 
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
    }
    @Override
    public Organization getOrganizationById(long id){
        try{
            Criteria c = getSession().createCriteria(Organization.class);
            c.add(Restrictions.eq("id", id));
            return (Organization) c.uniqueResult();
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
        
    }
    @Override
    public Organization getOrganizationByName(String name){
        Criteria c = getSession().createCriteria(Organization.class);
        c.add(Restrictions.eq("name", name));
        return (Organization) c.uniqueResult();
        
    }
    
    @Override
    public Organization getOrganizationByEmail(String email){
        try{
            Criteria c = getSession().createCriteria(Organization.class);
            c.add(Restrictions.eq("email", email));
            return (Organization) c.uniqueResult();
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
        
    }
    
    @Override
    public boolean checkOrgEmail(String email,String id){
        Query q=getSession().createSQLQuery("SELECT from `organization` WHERE `email`=:email AND `id` NOT LIKE :id");
        q.setString("email", email);
        q.setString("id", id);
       
        return q.uniqueResult()!=null;
    }
    
    @Override
    public boolean checkOrgName(String name,String id){
        Query q=getSession().createSQLQuery("SELECT from `organization` WHERE `name`=:name AND `id` NOT LIKE :id");
        q.setString("name", name);
        q.setString("id", id);
        return q.uniqueResult()!=null;
    }
    
    @Override
    @Transactional
    public void updateOrganization(Organization organization){
        getSession().update(organization);
    }
    @Override
    @Transactional
    public void deleteOrganizationById(String id){
        int id1 = Integer.parseInt(id);
        Query q = getSession().createSQLQuery("DELETE FROM `category` WHERE `id` = :id");
        q.setInteger("id", id1);
        q.executeUpdate();
        
    }
    @Override
    @Transactional
    public void setVerification(String id,String verification){
        Query q = getSession().createSQLQuery("UPDATE organization SET `verified` = :verification WHERE `id`=:id");
        q.setInteger("id", Integer.parseInt(id));
        q.setInteger("verification", Integer.parseInt(verification));
        q.executeUpdate();
        
    }
    @Override
    @Transactional
    public void setStatus(String id,String status){
        Query q = getSession().createSQLQuery("UPDATE organization SET `status` = :status WHERE `id`=:id");
        q.setInteger("id", Integer.parseInt(id));
        q.setInteger("status", Integer.parseInt(status));
        q.executeUpdate();
    }
    
     @Override
    public String loginAuth(String email,String password){
        
        try {
            Query qEmail;
            qEmail=getSession().createSQLQuery("SELECT `email` from `organization` where `email`=:email");
            qEmail.setString("email", email);
            Object mail=qEmail.uniqueResult();            
            if(mail==null){
                return "invalid email or password";
            }
            String emailverif=mail.toString();
            Query qUser,qPass;
            qPass=getSession().createSQLQuery("SELECT `password` from `organization` where `email`=:email");
            qPass.setString("email",email);
            List passList=qPass.list();
            String pass=(String) passList.get(0);
            //boolean match=passwordEncoder.matches(pass, user.getPassword());
            if(BCrypt.checkpw(password, pass)==false){
                return "invalid email or password";
            }
            //String password=user.getPassword();
            //System.out.println(org.getPassword());
            qUser=getSession().createSQLQuery("SELECT `id` from `organization` WHERE `email`=:email AND `password`=:password AND `verified`=:verified");
            qUser.setString("email",email);
            qUser.setString("password", pass);
            qUser.setParameter("verified", 1);
            String token;
            //String id=qUser.list().get(0).toString();
            String id;
            try{
                id=qUser.uniqueResult().toString();
            }
            catch(NullPointerException ex){
                System.out.println(ex.toString());
                return "email not verified";
            }
            /*Query test;
            test=getSession().createSQLQuery("SELECT `idOrg` from `token` WHERE `idOrg`=:id");
            test.setString("id",id);
            Object iduser=test.uniqueResult();
            if(iduser!=null){
                System.out.println("userexist");
                return "userexist";
            }*/
            long time=System.currentTimeMillis();
            String secret="inhlangano";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println(encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);   
            Date now=new Date(time);
            Date end=new Date(time+ 86400000);//1 day expiry time
            System.out.println(now);
            System.out.println(end);
            token = JWT.create()
                .withIssuer("http://socialclick.web.id").withIssuedAt(now).withExpiresAt(end)
                .withClaim("id", id).withClaim("access","organization").withClaim("admin", Boolean.FALSE)
                .sign(algorithm);//create JWT token with issuer auth0, and claims of orgnization's id in database
                //and access type of 'organization'
            /*q=getSession().createSQLQuery("INSERT into `token`(`idOrg`,`token`,`validTime`)VALUES (:id,:token,:time)");
            q.setString("id", id);
            q.setString("token", token);
            q.setString("time", (time+86400000) / 1000 + "");
            q.executeUpdate();*/
            System.out.println(token);
            return token;
        } catch (UnsupportedEncodingException exception){
            return exception.toString()+"error";
            //UTF-8 encoding not supported
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
            return exception.toString()+"error";
        }
        catch(EntityNotFoundException ex){
            System.out.println(ex.toString());
            return ex.toString()+"error";
        }
        catch(JWTDecodeException exception){
            System.out.println(exception.toString());
            return exception.toString()+"error";
        }
        catch(HibernateException ex){
            System.out.println(ex.toString());
            return ex.toString()+"error";
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return ex.toString()+"error";
        }
    }
    
    @Override
    @Transactional
    public String regisAuth(Organization org){
        try{
            String emailverif=org.getEmail();
            String token;
            Query q;
            q=getSession().createSQLQuery("SELECT `id` from `organization` WHERE `email`=:email");
            q.setString("email", org.getEmail());
            String id=q.list().get(0).toString();
            long time=System.currentTimeMillis();
            String secret="inhlangano";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println(encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);   
            Date now=new Date(time);
            Date end=new Date(time+ 86400000);//1 day expiry time
            System.out.println(now);
            System.out.println(end);
            token = JWT.create()
                .withIssuer("http://socialclick.web.id").withIssuedAt(now).withExpiresAt(end)
                .withClaim("id",id).withClaim("access","organization").withClaim("admin", Boolean.FALSE)
                .sign(algorithm);//create JWT token with issuer auth0, and claims of organization's id in database
                //and access type of 'organization'
            /*q=getSession().createSQLQuery("INSERT into `token`(`idOrg`,`token`,`validTime`)VALUES (:id,:token,:time)");
            q.setString("id", id);
            q.setString("token", token);
            q.setString("time", (time+86400000) / 1000 + "");
            q.executeUpdate();*/
            //System.out.println(token);
            return token;
        }catch (UnsupportedEncodingException exception){
            return exception.toString();
            //UTF-8 encoding not supported
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
            return exception.toString();
        } 
    }
    
    @Override
    public String getToken(String id){
        Query q;
        q=getSession().createSQLQuery("SELECT `token` from `token` WHERE `idOrg`=:id");
        q.setString("id",id);
        List list=q.list();
        String token=list.get(0).toString();
        return token;
    }
    
    @Override
    public String validateOrg(String token,String id){
        try {
            String secret="inhlangano";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println("valid "+encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);
            JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("http://socialclick.web.id")
                .withClaim("access","organization").withClaim("id",id).withClaim("admin", Boolean.FALSE)    
                .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);
            Map<String, Claim> claims = jwt.getClaims();    //Key is the Claim name
            String claim = claims.get("access").toString();
            return "valid";
        } 
        catch (UnsupportedEncodingException exception){
            //UTF-8 encoding not supported
            System.out.println(exception.toString());
            return exception.toString();
        } 
        catch (InvalidClaimException exception){
            //Invalid signature/claims           
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/            
            return "invalid claim";
        }
        catch (TokenExpiredException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/
            return "token expired";
        }
        catch(AlgorithmMismatchException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/            
            return "invalid algorithm used";
        }
        catch(JWTDecodeException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/
            return "error decoding token";
        }
        catch(SignatureVerificationException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/
            return "error verifying signature";
        }        
    }
    
    @Override
    public String returnId(String email){
            Query qUser;
            qUser=getSession().createSQLQuery("SELECT `id` from `organization` WHERE `email`=:email");
            qUser.setString("email",email);
            List list=qUser.list();
            String id=list.get(0).toString();
            return id;
    }
    
}
