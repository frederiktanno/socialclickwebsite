/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.dao;

import com.mycompany.socialclickwebsite.models.Gallery;
import com.mycompany.socialclickwebsite.models.Project;
import com.mycompany.socialclickwebsite.models.ProjectMember;
import com.mycompany.socialclickwebsite.models.Users;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marvint11
 */
@Repository("ProjectDao")
public class ProjectDaoImpl extends AbstractDao implements ProjectDao{
    DateFormat date=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    @Override
    @Transactional
    public String saveUserProject(Project project,String id){
        /*converts regdeadline and datestart string values to timestamp, then to millisecond
          this is done so that the software can compare the time values of regdeadline and datestart
          to reject applicants who applies after the regdeadline
          then inserts all data into database
        */
        long month=86400000*30;//1 month time limit
        long today=System.currentTimeMillis();//get current date
        Date dateregdeadline;
        Date datedatestart;
        String regdeadline=project.getRegdeadline();
        String datestart=project.getDatetime();
        try {
             dateregdeadline=date.parse(regdeadline);//convert string to Date
             System.out.println("regdeadline: "+dateregdeadline);
             long regdeadlinemillis=dateregdeadline.getTime();//convert to millisecond
             System.out.println(regdeadlinemillis);
             project.setRegdeadline(String.valueOf(regdeadlinemillis));
             datedatestart=date.parse(datestart);
             long datestartmillis=datedatestart.getTime();
             project.setDatetime(String.valueOf(datestartmillis));
             if(datestartmillis<=today){
                 return "error: invalid project start date";
             }
             else if(datestartmillis-(today+month)<=0){
                 return "error: cannot start project in under a month";
             }
             try{
                persist(project);
                String idProject=String.valueOf(project.getId());
                System.out.println(idProject);
               Query q=getSession().createSQLQuery("INSERT into `projectinituser` (`idProject`,`idUser`)VALUES"
                    + "(:idProject,:id)");
                q.setString("idProject", idProject);
                q.setParameter("id",id);
                q.executeUpdate();
                q=getSession().createSQLQuery("INSERT into `projectmember` (`idProject`,`idUser`,`hours`,`approved`)"
                        + "VALUES (:idProject,:id,:hours,:approved)");
                q.setString("idProject", idProject);
                q.setString("id",id);
                q.setParameter("hours",0);
                q.setString("approved", "1");
                q.executeUpdate();
                return "ok";
            }
            catch(Exception ex){
                System.out.println(ex.toString());
                return "error";
            }
        }
        
        catch (ParseException ex) {
            Logger.getLogger(ProjectDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "error parsing";
        }
        
       
        /*long timenow=System.currentTimeMillis();
        if(regdeadlinemillis>timenow){
            System.out.println("valid");
        }
        else{
            System.out.println("not valid");
        }*/       
    }
    
    @Override
    @Transactional
    public String saveOrgProject(Project project,String id){
        /*converts regdeadline and datestart string values to timestamp, then to millisecond
          this is done so that the software can compare the time values of regdeadline and datestart
          to reject applicants who applies after the regdeadline
          then inserts all data into database
        */
        long month=86400000*30;//1 month time limit
        long today=System.currentTimeMillis();//get current date
        Date dateregdeadline;
        Date datedatestart;
        String regdeadline=project.getRegdeadline();
        String datestart=project.getDatetime();
        try {
             dateregdeadline=date.parse(regdeadline);//convert string to Date
             System.out.println("regdeadline: "+dateregdeadline);
             long regdeadlinemillis=dateregdeadline.getTime();//convert to millisecond
             System.out.println(regdeadlinemillis);
             project.setRegdeadline(String.valueOf(regdeadlinemillis));
             datedatestart=date.parse(datestart);
             long datestartmillis=datedatestart.getTime();
             project.setDatetime(String.valueOf(datestartmillis));
            if(datestartmillis<=today){
                 return "error: invalid project start date";
             }
            else if(datestartmillis-(today+month)<=0){
                 return "error: cannot start project in under a month";
             }
             
            try{
                persist(project);
                String idProject=String.valueOf(project.getId());
                System.out.println(idProject);
               Query q=getSession().createSQLQuery("INSERT into `projectinitorg` (`idProject`,`idOrg`)VALUES"
                    + "(:idProject,:id)");
                q.setString("idProject", idProject);
                q.setParameter("id",id);
                q.executeUpdate(); 
                return "ok";
            }
            catch(Exception ex){
                System.out.println(ex.toString());
                return "error";
            }
        }
        catch (ParseException ex) {
            Logger.getLogger(ProjectDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
            return ex.toString()+"eror";
        }
        /*q=getSession().createSQLQuery("INSERT into `projectmember` (`idProject`,`idUser`,`hours`,`approved`)"
                + "VALUES (:idProject,:id,:hours,:approved)");
        q.setString("idProject", idProject);
        q.setString("id",id);
        q.setParameter("hours",0);
        q.setParameter("hours",1);
        q.setString("approved", "0");
        q.executeUpdate();*/
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Project>getProject(){
        /* gets a list of projects using Hibernate Criteria, only loads the selected properties*/
        try{
            //Example example=Example.create(Project.class).excludeProperty("projMember").excludeZeroes().ignoreCase().enableLike();
            Criteria c = getSession().createCriteria(Project.class)
                    .setProjection(Projections.projectionList()
                    .add(Projections.property("id"), "id")
                    .add(Projections.property("name"), "name")
                    .add(Projections.property("description"),"description")
                    .add(Projections.property("date"),"date")
                    .add(Projections.property("location"),"location")
                    .add(Projections.property("regdeadline"),"regdeadline")
                    .add(Projections.property("maxmembers"),"maxmembers")
                    .add(Projections.property("date"),"date") 
                    .add(Projections.property("users"),"users")
                    .add(Projections.property("organization"),"organization")
                    .add(Projections.property("profile"),"profile")
                    .add(Projections.property("category"),"category")
                    .add(Projections.property("hours"),"hours"))
                    .setResultTransformer(Transformers.aliasToBean(Project.class));
            List<Project>projects=c.list();
            return projects;
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            return null;
        }
        
    }
    @Override
    public Project getProjectById(long id){
        try{
            /*Criteria c = getSession().createCriteria(Project.class).setFetchMode("projMember", FetchMode.JOIN)
                    .createAlias("projMember", "pm")
                    .add(Restrictions.eq("id", id)).add(Restrictions.eq("pm.approve", 1));
            Project project=(Project) c.uniqueResult();*/
            /* 
                trying to fetch a Project pojo using hql, hopefully can add some restrictions in
                fetching ProjectMembers
            */
            Query q=getSession().createQuery("from Project p left join"
                    + " fetch p.projMember pm where pm.approve like :approved and p.id like :id");
            q.setParameter("id",id);
            q.setParameter("approved",1);
            Project project=(Project) q.list().get(0);
            
            //Hibernate.initialize(project.getProjMember());           
            //Hibernate.initialize(project.getOrgInit());
            
            return project;
        }
        catch(NullPointerException |IndexOutOfBoundsException ex){
            try{
                Query q=getSession().createQuery("from Project p left join fetch p.projMember pm where p.id=:id");
                q.setParameter("id",id);
                Project project=(Project) q.list().get(0);
                return project;
            }
            catch(NullPointerException | IndexOutOfBoundsException | HibernateException exd){
                System.out.println(exd.toString());
                return null;
            }
        }
    }
    @Override
    public Object getProjectByName(String name){
        long id=Long.parseLong(name);
        System.out.println(id);
        try{
            /*Criteria c = getSession().createCriteria(Project.class).setFetchMode("projMember", FetchMode.JOIN).setFetchMode("users", FetchMode.JOIN)
                    .setFetchMode("organization", FetchMode.JOIN)
                    .createAlias("projMember", "pm").createAlias("users", "u").createAlias("organization", "o")
                    .setProjection(Projections.projectionList()
                    .add(Projections.property("id"), "id")
                    .add(Projections.property("name"), "name")
                    .add(Projections.property("description"),"description")
                    .add(Projections.property("date"),"date")
                    .add(Projections.property("location"),"location")
                    .add(Projections.property("regdeadline"),"regdeadline")
                    .add(Projections.property("maxmembers"),"maxmembers")
                    .add(Projections.property("date"),"date") 
                    .add(Projections.property("users"),"users")
                    .add(Projections.property("organization"),"organization")
                    .add(Projections.property("profile"),"profile")
                    .add(Projections.property("category"),"category")
                    .add(Projections.property("hours"),"hours"))
                    .setResultTransformer(Transformers.aliasToBean(Project.class))
                    .add(Restrictions.eq("id", id)).add(Restrictions.eq("pm.approve", 1));
            Project project=(Project) c.uniqueResult();*/
            /* 
                trying to fetch a Project pojo using hql, hopefully can add some restrictions in
                fetching ProjectMembers
            */
            Query q=getSession().createQuery("select p.id,p.name,p.description,p.category.id,p.category.category,p.date,p.finished,p.location,p.profile,"
                    + "p.regdeadline,p.rating,p.maxmembers,p.hours,p.organization.name,p.organization.id, p.users.id, "
                    + "p.users.fname, p.users.lname, pm.users.fname, pm.users.lname, pm.users.id, pm.approve from Project p left join"
                    + "  p.projMember pm where pm.approve like :approved and p.id like :id");
            q.setParameter("id",id);
            q.setParameter("approved",1);
            Object project= q.list().get(0);
            
            //Hibernate.initialize(project.getProjMember());           
            //Hibernate.initialize(project.getOrgInit());
            
            return project;
        }
        catch(NullPointerException |IndexOutOfBoundsException ex){
            try{
                Query q=getSession().createQuery("select p.id,p.name,p.description,p.category.id,p.category.category,p.date,p.finished,p.location,p.profile,"
                    + "p.regdeadline,p.rating,p.maxmembers,p.hours,p.organization.name,p.organization.id, p.users.id, "
                    + "p.users.fname, p.users.lname, pm.users.fname, pm.users.lname, pm.users.id, pm.approve from Project p left join"
                    + "  p.projMember pm where p.id like :id");
                q.setParameter("id",id);
                Object project= q.list().get(0);
                return project;
            }
            catch(NullPointerException | IndexOutOfBoundsException | HibernateException exd){
                System.out.println(exd.toString());
                return null;
            }
        }
    }
    @Override
    @Transactional
    public String updateProject(Project project){
        try{
            getSession().update(project);
            return "ok";
        }
        catch(Exception ex){
            return ex.toString();
        }
        
    }
    @Override
    @Transactional
    public void deleteProjectById(String id){
        try{
            Query q = getSession().createSQLQuery("DELETE FROM `category` WHERE `id` = :id");
            q.setInteger("id", Integer.parseInt(id));
            q.executeUpdate();
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
        }
    }
    
    @Override
    public List<Project> getJoinedProject(String id){
        /* first, fetch a project's ID from projectmember table where the user is a member in
           then the function will return a null if user hasn't joined any projects
           else, the function runs a try and catch statements where the it will try to fetch all projects which
           the user has joined
           the fetched object from the database needs to be converted into an object type of Project manually
           and assigns the values of Project object members according to the result of the query
        */
        Query q=getSession().createSQLQuery("SELECT `idProject`from `projectmember` WHERE `idUser`=:id AND `approved`=:approved");
        
        q.setParameter("id",id);
        
        q.setParameter("approved",1);
        List list=q.list();
        List<Project> projects = new ArrayList<>();
        int i=0;
        if(list.size()<1){
            return projects;
        }
        try{
           while(i<list.size()){
                String idproject=list.get(i).toString();
                long idlongproject=Long.parseLong(idproject);
                System.out.println("1");
                Criteria c = getSession().createCriteria(Project.class).add(Restrictions.eq("id", idlongproject))
                    .setProjection(Projections.projectionList()
                    .add(Projections.property("id"), "id")
                    .add(Projections.property("name"), "name")
                    .add(Projections.property("description"),"description")
                    .add(Projections.property("date"),"date")
                    .add(Projections.property("location"),"location")
                    .add(Projections.property("regdeadline"),"regdeadline")
                    .add(Projections.property("maxmembers"),"maxmembers")
                    .add(Projections.property("date"),"date") 
                    .add(Projections.property("users"),"users")
                    .add(Projections.property("organization"),"organization")
                    .add(Projections.property("profile"),"profile")
                    .add(Projections.property("category"),"category")
                    .add(Projections.property("hours"),"hours"))
                    .setResultTransformer(Transformers.aliasToBean(Project.class));
                projects=c.list();
                i++;
            }
           return projects;
        }       
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
        catch (Exception ex){
            System.out.println(ex.toString());
            return null;
        }
    }
    
    @Override
    public List<Project> getUserIniatedProject(String id){
        /*
            first select the project id initialized by the user
            then select the project details with id received from the previous list
            details will be returned in a List of Object datatype, so you need to
            iterate the list, create a new instance of Project, and set the values
            of Project with the retrieved data from the List of Object
        */
        Query q=getSession().createSQLQuery("SELECT `idProject` from `projectinituser` WHERE `idUser`=:idUser");
            q.setString("idUser", id);
            List idProjects=q.list();
            List<Project> projectList = new ArrayList<>();
            System.out.println(idProjects);
            if(idProjects.size()<1){
                return projectList;
            }
            int i=0;
        try{
            if(idProjects.size()==1){
                String idproject=idProjects.get(i).toString();
                long idlongproject=Long.parseLong(idproject);
                System.out.println(idproject);
                 Criteria c = getSession().createCriteria(Project.class).add(Restrictions.eq("id", idlongproject))
                    .add(Restrictions.eq("finished", 0))
                    .setProjection(Projections.projectionList()
                    .add(Projections.property("id"), "id")
                    .add(Projections.property("name"), "name")
                    .add(Projections.property("description"),"description")
                    .add(Projections.property("date"),"date")
                    .add(Projections.property("location"),"location")
                    .add(Projections.property("regdeadline"),"regdeadline")
                    .add(Projections.property("maxmembers"),"maxmembers")
                    .add(Projections.property("date"),"date") 
                    .add(Projections.property("users"),"users")
                    .add(Projections.property("organization"),"organization")
                    .add(Projections.property("profile"),"profile")
                    .add(Projections.property("category"),"category")
                    .add(Projections.property("hours"),"hours"))
                    .setResultTransformer(Transformers.aliasToBean(Project.class));
                 projectList=c.list();
            }
            else{
                while(i<idProjects.size()){
                    String idproject=idProjects.get(i).toString();
                    long idlongproject=Long.parseLong(idproject);
                    System.out.println(idproject);
                    Criteria c = getSession().createCriteria(Project.class).add(Restrictions.eq("id", idlongproject))
                    .setProjection(Projections.projectionList()
                    .add(Projections.property("id"), "id")
                    .add(Projections.property("name"), "name")
                    .add(Projections.property("description"),"description")
                    .add(Projections.property("date"),"date")
                    .add(Projections.property("location"),"location")
                    .add(Projections.property("regdeadline"),"regdeadline")
                    .add(Projections.property("maxmembers"),"maxmembers")
                    .add(Projections.property("date"),"date") 
                    .add(Projections.property("users"),"users")
                    .add(Projections.property("organization"),"organization")
                    .add(Projections.property("profile"),"profile")
                    .add(Projections.property("category"),"category")
                    .add(Projections.property("hours"),"hours"))
                    .setResultTransformer(Transformers.aliasToBean(Project.class));
                 projectList=c.list();
                    i++;
                } 
            }
            return projectList;
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
        catch (Exception ex){
            System.out.println(ex.toString());
            return null;
        }
        /*List<Project>projectList;
        try{
            
            Query q=getSession().createSQLQuery("SELECT * from `project` where `id`=(SELECT `idProject` from `projectinituser`"
                    + "WHERE `idUser`=:id) AND `finished`=0");
            q.setString("id",id);
            projectList=(List<Project>)q.list();
            if(projectList.size()<1){
                return null;
            }
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }*/      
    }
        
    
    @Override
    @Transactional
    public String joinProject(String id,String idProject){
        try{
            //first, check if user has joined a project or not
            Query q=getSession().createSQLQuery("SELECT `idUser` from `projectmember`where `idUser`=:id AND `idProject` IN"
                + "(SELECT `id` from `project`WHERE `finished`=:finished)");
            q.setParameter("id",id);
            q.setParameter("id",idProject);
            q.setParameter("finished",0);
            List joined=q.list();
            System.out.println(joined);
            if(joined.isEmpty()==false){
                return "error: you have joined some projects";
            }
            //check the registration date with the deadline date
            q=getSession().createSQLQuery("SELECT `regdeadline` from `project` where `id`=:id");
            q.setParameter("id", idProject);
            String regdeadline=(String) q.list().get(0);
            System.out.println(regdeadline);
            long regdeadlinelong=Long.parseLong(regdeadline);
            System.out.println(regdeadlinelong);
            long registration=System.currentTimeMillis();
            System.out.println(registration);
            if(registration>=regdeadlinelong){
                return "error: you cannot register to this project";
            }
            //register the user into the project
            q=getSession().createSQLQuery("INSERT into `projectmember`(`idUser`,`idProject`,`hours`,`approved`) VALUES (:id,:idProject,:hours,:accepted)");
            q.setString("id", id);
            q.setString("idProject", idProject);
            q.setParameter("hours",0);
            q.setParameter("accepted",0);
            q.executeUpdate();
            return "ok";
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            return ex.toString()+"error";
        }
        }
       
    
    @Override
    public int countProjectMembers(String id){
        try{
           Query q=getSession().createSQLQuery("SELECT COUNT(*) as count from `projectmember` WHERE `idProject`=:id AND `approved`=1")
                   .addScalar("count", LongType.INSTANCE);
            q.setString("id", id);
            int count=((Long)q.uniqueResult()).intValue();
            return count; 
        }
       catch(HibernateException ex){
           System.out.println(ex);
           return 99999;
       }
    }
    
    @Override
    public List<Gallery>getProjectGallery(String id){
        /*Query q=getSession().createSQLQuery("SELECT `photo` from `gallery` WHERE `idProject`=:id");
        q.setString("id", id);
        List gallery=q.list();*/
        Criteria c = getSession().createCriteria(Gallery.class);
        c.add(Restrictions.eq("idProject", id));
        return (List<Gallery>) c.list();
    }
    
    @Override
    public List<Project>getOrgInitiatedProject(String id){
        /*
            first select the project id initialized by the organisation
            then select the project details with id received from the previous list
            details will be returned in a List of Object datatype, so you need to
            iterate the list, create a new instance of Project, and set the values
            of Project with the retrieved data from the List of Object
        */
        
        Query q=getSession().createSQLQuery("SELECT `idProject` from `projectinitorg` WHERE `idOrg`=:idOrg");
            q.setString("idOrg", id);
            List idProjects=q.list();
            if(idProjects.size()<1){
                return null;
            }
            int i=0;
            List<Project> projectList = new ArrayList<>();
        try{
           while(i<idProjects.size()){
                String idproject=idProjects.get(i).toString();           
                q=getSession().createSQLQuery("SELECT * from `project` where `id`=:id");
                q.setString("id", idproject);
                List<Object> result = q.list(); 
                    Iterator itr = result.iterator();
                    while(itr.hasNext()){
                       Object[] obj = (Object[]) itr.next();
                       Project project = new Project();
                       project.setId(Long.parseLong(String.valueOf(obj[0])));
                       project.setName(String.valueOf(obj[1]));
                       project.setDescription(String.valueOf(obj[2]));
                       project.setDatetime(String.valueOf(obj[3]));
                       project.setLocation(String.valueOf(obj[4]));
                       project.setRegdeadline(String.valueOf(obj[5]));
                       project.setMaxmembers(Integer.parseInt(String.valueOf(obj[6])));
                       project.setRating(Float.parseFloat(String.valueOf(obj[7])));
                       project.setFinished(Integer.parseInt(String.valueOf(obj[9])));
                       projectList.add(project);
                    }
                i++;
            } 
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
        return projectList;
    }
    
    @Override
    @Transactional
    public String approveApplicant(String id,String idProject){
        try{
           Query q=getSession().createSQLQuery("UPDATE `projectmember` SET `approved`=:approved WHERE `idUser`=:id "
                + "AND `idProject`=:idProject");
            q.setString("id", id);
            q.setString("idProject",idProject);
            q.setParameter("approved", 1);
            return "approved"; 
        }
        catch(HibernateException ex){
            return ex.toString();
        }
    }
    
    @Override
    @Transactional
    public String rejectApplicant(String id,String idProject){
        try{
           Query q=getSession().createSQLQuery("UPDATE `projectmember` SET `approved`=:approved WHERE `idUser`=:id "
                + "AND `idProject`=:idProject");
            q.setString("id", id);
            q.setString("idProject",idProject);
            q.setParameter("approved", 2);
            return "rejected"; 
        }
        catch(HibernateException ex){
            return ex.toString();
        }
    }
    
    @Override
    public int countAppliedUsers(String idProject){
        try{
           Query q=getSession().createSQLQuery("SELECT COUNT (*) as count FROM `projectmember` WHERE `idProject`=:idProject")
                   .addScalar("count", LongType.INSTANCE);
            q.setString("idProject",idProject);
            int count=((Long)q.uniqueResult()).intValue();
            if(count>50){
                return 999;
            }
            return count; 
        }
        catch(HibernateException ex){
            return 9999;
        }
    }
    
    @Override
    public int countUserCreatedProjects(String id){
        try{
           Query q=getSession().createSQLQuery("SELECT COUNT(*) as count from `projectinituser` WHERE `idUser`=:id")
                   .addScalar("count", LongType.INSTANCE);
            q.setString("idUser", id);
            int count=((Long)q.uniqueResult()).intValue();
            return count; 
        }
       catch(HibernateException ex){
           System.out.println(ex);
           return 99999;
       }
    }
    
    @Override
    public int countOrgCreatedProjects(String id){
        try{
           Query q=getSession().createSQLQuery("SELECT COUNT(*) as count from `projectinitorg` WHERE `idOrg`=:id")
                   .addScalar("count", LongType.INSTANCE);
            q.setString("idOrg", id);
            int count=((Long)q.uniqueResult()).intValue();
            return count; 
        }
       catch(HibernateException ex){
           System.out.println(ex);
           return 99999;
       }
    }
    
    @Override
    public int countJoinedProjects(String id){
         try{
           Query q=getSession().createSQLQuery("SELECT COUNT(*) as count from `projectmember` WHERE `idUser`=:id AND `approved`=1")
                   .addScalar("count", LongType.INSTANCE);
            q.setString("idUser", id);
            int count=((Long)q.uniqueResult()).intValue();
            return count; 
        }
       catch(HibernateException ex){
           System.out.println(ex);
           return 99999;
       }
    }
    
    @Override
    @Transactional
    public String setProjectStatus(String idProject){
        try{
           Query q=getSession().createSQLQuery("UPDATE `project` SET `finished`=:finished `WHERE` `idProject`:idProject");
            q.setString("idProject",idProject);
            q.setParameter("finished", 1);
            return "ok"; 
        }
        catch(HibernateException ex){
            return ex.toString();
        }
    }
    
    @Override
    public int countTotalHours(String id){
        try{
           Query q=getSession().createSQLQuery("SELECT cast(sum(`hours`)as signed) from `projectmember`where `idUser`=:id");
            q.setString("id", "1");
            //q.setParameter("approved","1");
            BigInteger counts=(BigInteger) q.list().get(0);
            int count=counts.intValue();
            System.out.println(count);
            return count; 
        }
       catch(HibernateException ex){
           System.out.println(ex);
           return 99999;
       }
    }
    
    @Override
    public List<ProjectMember>getProjectMembers(String id){
        try{
            /*Criteria c=getSession().createCriteria(ProjectMember.class);
            c.add(Restrictions.eq("project", id));
            List<ProjectMember>projMem=c.list();*/
            /*Query q=getSession().createSQLQuery("SELECT * from `user` WHERE `id` IN "
                    + "(SELECT `idUser` from `projectmember` where `idProject`=:id)");*/
            Query q=getSession().createSQLQuery("SELECT * from `projectmember` where `idProject`=:id");
            q.setParameter("id",id);
            List<ProjectMember>projMem=q.list();
            if(projMem.isEmpty()==true){
               return projMem; 
            }
            return projMem;            
        }
        catch(Exception ex){
            System.out.println(ex);
            return null;
        }
    }
    
    @Override
    public List<Project>getProjectsByCategory(String[]idCategory){
        //get all projects
        Criteria c = getSession().createCriteria(Project.class)
                .setProjection(Projections.projectionList()
                    .add(Projections.property("id"), "id")
                    .add(Projections.property("name"), "name")
                    .add(Projections.property("description"),"description")
                    .add(Projections.property("date"),"date")
                    .add(Projections.property("location"),"location")
                    .add(Projections.property("regdeadline"),"regdeadline")
                    .add(Projections.property("maxmembers"),"maxmembers")
                    .add(Projections.property("date"),"date") 
                    .add(Projections.property("users"),"users")
                    .add(Projections.property("organization"),"organization")
                    .add(Projections.property("profile"),"profile")
                    .add(Projections.property("category"),"category")
                    .add(Projections.property("hours"),"hours"))
                    .setResultTransformer(Transformers.aliasToBean(Project.class));
        List<Project>projects=c.list();      
        System.out.println(projects.size());
        if(projects.isEmpty()==true){
            return projects;
        }
        List<Project>selectedProjects=new ArrayList<>();
        Project proj;
        try{
            System.out.println("filtering projects");
            //filter projects by category
            for(int i=0;i<idCategory.length;i++){
                //get id category
                int id=Integer.parseInt(idCategory[i]);
                System.out.println(id);
                
                //traverse through list of projects
                for(int s=0;s<projects.size();s++){
                    proj=projects.get(s);
                    System.out.println(projects.get(s).getCategory().getId());
                    System.out.println(proj);
                    if(proj.getCategory().getCategory().equals(idCategory[i])){
                        selectedProjects.add(proj);
                    }
                }
            }
            System.out.println(selectedProjects);
           return selectedProjects; 
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            return null;
        }
        
    }
    
    @Override
    @Transactional
    public String finishProject(String id,int hours){
        long idProject=Long.parseLong(id);
        try{
            //update status of project to finished
            Query q=getSession().createSQLQuery("update `project` set `finished`=:finished "
                    + "and `hours` =:hours where `id`=:id");
            q.setParameter("id",idProject);
            q.setParameter("hours",hours);
            q.setParameter("finished",1);
            q.executeUpdate();
            //fetch the list of project members based on the referenced projectID 
            Criteria c = getSession().createCriteria(ProjectMember.class).setFetchMode("project", FetchMode.JOIN)
                    .createAlias("project", "p")
                    .add(Restrictions.eq("p.id", idProject));            
            List<ProjectMember>project=c.list();
            
            if(project==null){
                return "error";
            }
            //project.setFinished(1);
            //set the new amount of user's project hours according to the total hours of the project
            for(int i=0;i<project.size();i++){
                project.get(i).getMember().setHours(project.get(i).getHour()+hours);
            }
            return "ok";
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            return ex.toString()+"error";
        }
    
        
    }
}
