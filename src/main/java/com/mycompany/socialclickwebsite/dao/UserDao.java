/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.dao;

import com.mycompany.socialclickwebsite.models.Bookmark;
import com.mycompany.socialclickwebsite.models.Project;
import com.mycompany.socialclickwebsite.models.Token;
import com.mycompany.socialclickwebsite.models.Users;
import java.util.List;

/**
 *
 * @author Marvint11
 */
public interface UserDao {
    String saveData(Users data);
    List<Users>getAllData();
    void deleteUserById(String id);
    void updateUser(Users data);
    Users findByName(String name);
    Users findById(long id);
    String loginAuth(String email, String password);
    boolean checkEmail(String email,String id);
    String regisAuth(Users user);
    String getToken(String id);
    String validateUser(String token,String id);
    String returnId(String email);
    void logout(String token,String id);
    String rehash(String id);
    String validateValidationToken(String token,long id);
    String createToken(long id);
    String checkDOB(String dob);
    boolean checkIdNum(String idnum);
    String validateResetToken(String token,long id);
    String resetPassword(String email,String password);
}
