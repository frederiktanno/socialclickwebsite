/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.dao;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.*;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.mycompany.socialclickwebsite.models.Bookmark;
import com.mycompany.socialclickwebsite.models.Interest;
import com.mycompany.socialclickwebsite.models.Project;
import java.io.UnsupportedEncodingException;
import java.util.List;
import com.mycompany.socialclickwebsite.models.Users;
import com.mycompany.socialclickwebsite.models.Token;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityNotFoundException;
import javax.xml.bind.DatatypeConverter;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author Marvint11
 */
@Repository("UserDao")
public class UserDaoImpl extends AbstractDao implements UserDao{
   @Autowired
    private PasswordEncoder passwordEncoder;
   
   //final Map<String,Object>claims=new HashMap();
   final String issuer="http://socialclick.web.id";
   final String access="user";
   String emailtoken;
   String idtoken;
   Date now;
   Date end;
   DateFormat date=new SimpleDateFormat("dd/MM/yyyy");
    
    @Override
    @Transactional
    public String saveData(Users data){
        try{
           persist(data);
           return "ok";
        }
        catch(HibernateException ex){
            System.out.println(ex.toString());
            return ex.toString()+"error";
        }
    }
    @Override
    public List<Users>getAllData(){
        try{
            Criteria c=getSession().createCriteria(Users.class)
                    .setProjection(Projections.projectionList()
                    .add(Projections.property("id"), "id")
                    .add(Projections.property("fname"), "fname")
                    .add(Projections.property("lname"), "lname")
                    .add(Projections.property("address"), "address")
                    .add(Projections.property("phone"), "phone")
                    .add(Projections.property("name"), "name")
                    .add(Projections.property("interest"), "interest"));
            List<Users>users=c.list();
            return users;
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
    }
    @Override
    public Users findByName(String user){
        try{
            Criteria criteria=getSession().createCriteria(Users.class);
            criteria.add(Restrictions.eq("email", user));
            return (Users)criteria.uniqueResult();
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            return null;
        }
        
    }
    
    @Override
    public Users findById(long id){
        try{
            
           Criteria criteria=getSession().createCriteria(Users.class);
            criteria.add(Restrictions.eq("id", id));

            Users user=(Users)criteria.uniqueResult();
            //Hibernate.initialize(user.getInterest());//initialize session to fetch
            //list of user's interests from userinterest join table, because it was lazily fetched
            //recommended only if the proxy object does not contain lots of details
            return user; 
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return null;
        }
        
    }
    
    @Override
    public boolean checkIdNum(String idnum){
        Query q=getSession().createSQLQuery("SELECT `id` from `user` WHERE `idnum`=:idnum");
        q.setString("idnum", idnum);
        return q.uniqueResult()!=null;
    }
    @Override
    public boolean checkEmail(String email,String id){
        Query q=getSession().createSQLQuery("SELECT * from `user` WHERE `email`=:email AND `id` NOT LIKE :id");
        q.setString("email", email);
        q.setString("id", id);
       
        return q.uniqueResult()!=null;
    }
    
    @Override
    @Transactional
    public void deleteUserById(String id){
        Query query=getSession().createSQLQuery("DELETE from `user` WHERE `id`=:id");
        query.setString("id", id);
        query.executeUpdate();
    }
    
    @Override
    @Transactional
    public void updateUser(Users user){
        getSession().update(user);
    }
    
    @Override
    public String loginAuth(String email, String password){
        /*  
         In logging in, check if email exists in the database, then match the inputted password with the password
         stored in database
         if the credentials are correct, create a JWT token with a secret and access level of user
         and expiry time
        */
        try {
            Query qEmail;
            qEmail=getSession().createSQLQuery("SELECT `email` from `user` where `email`=:email");
            qEmail.setString("email", email);
            Object mail=qEmail.uniqueResult();           
            if(mail==null){
                return "invalid email or password";
            }
            String emailverif=mail.toString();
            Query qUser,qPass,qStatus;
            qPass=getSession().createSQLQuery("SELECT `password` from `user` where `email`=:email");
            qPass.setString("email",email);
            List passList=qPass.list();
            String pass=(String) passList.get(0);
            System.out.println(pass);
            System.out.println(password);
            //boolean match=passwordEncoder.matches(pass, user.getPassword());
            if(BCrypt.checkpw(password, pass)==false){
                return "invalid email or password";
            }

            qUser=getSession().createSQLQuery("SELECT `id` from `user` WHERE `email`=:email AND `password`=:password"
                    + " AND `accessLevel`=:accesslevel AND `status`=:status");
            qUser.setString("email",email);
            qUser.setString("password", pass);
            qUser.setString("accesslevel","0");
            qUser.setParameter("status",1);
            //List list=qUser.list();//gets user id with email and password credential
            String token;
            
            String id;
            try{
                id=qUser.uniqueResult().toString();
            }
            catch(NullPointerException ex){
                System.out.println(ex.toString());
                return "email not verified";
            }
            
            //check if user is blocked or not
            Query q=getSession ().createSQLQuery("SELECT `status` from `user` where `id`=:id");
            q.setParameter("id",id);
            int status= (int) q.list().get(0);
            if(status==2){
                return "blocked";
            }
            
            long time=System.currentTimeMillis();
            String secret="umsebenzisi";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println(encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);   
            now=new Date(time);
            end=new Date(time+ 86400000);//1 day expiry time
            System.out.println(now);
            System.out.println(end);
            idtoken=id;
            emailtoken=emailverif;
            token = JWT.create()
                .withIssuer(issuer).withIssuedAt(now).withExpiresAt(end)
                .withClaim("id", id).withClaim("access",access).withClaim("admin", Boolean.FALSE)
                .sign(algorithm);//create JWT token with issuer auth0, and claims of user's id in database
                //and access type of 'user'
             //claims.put("id",idtoken);
             //claims.put("access",access);
           /* q=getSession().createSQLQuery("INSERT into `token`(`idUser`,`token`,`validTime`)VALUES (:id,:token,:time)");
            q.setString("id", id);
            q.setString("token", token);
            //q.setString("time", (time + 3600000) / 1000 + "");
            q.setString("time",(time+86400000)/1000+"");
            q.executeUpdate();*/
            System.out.println(token);
            return token;
        } catch (UnsupportedEncodingException exception){
            return exception.toString();
            //UTF-8 encoding not supported
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
            System.out.println(exception.toString());
            return exception.toString()+" error";
        } 
        catch(EntityNotFoundException ex){
            System.out.println(ex.toString());
            return ex.toString()+" error";
        }
        catch(JWTDecodeException exception){
            System.out.println(exception.toString());
            return exception.toString()+" error";
        }
        catch(HibernateException ex){
            System.out.println(ex.toString());
            return ex.toString()+" error";
        }
        catch(NullPointerException ex){
            System.out.println(ex.toString());
            return ex.toString()+" error";
        }
    }
    
    @Override
    @Transactional
    public String regisAuth(Users user){
        try{
            String emailverif=user.getEmail();
            String token;
            Query q;
            q=getSession().createSQLQuery("SELECT `id` from `user` WHERE `email`=:email");
            q.setString("email", user.getEmail());
            String id=q.list().get(0).toString();
            long time=System.currentTimeMillis();
            String secret="umsebenzisi";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println(encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);
            now=new Date(time);
            end=new Date(time+ 86400000);//1 day expiry time
            System.out.println(now);
            System.out.println(end);
            emailtoken=emailverif;
            token = JWT.create()
                .withIssuer("http://socialclick.web.id").withIssuedAt(now).withExpiresAt(end)
                .withClaim("access","user").withClaim("admin", Boolean.FALSE)
                .withClaim("verified", Boolean.FALSE).withClaim("email", emailtoken)
                .sign(algorithm);
                //create JWT token with issuer , and claims of user's id in database
                //and access type of 'user'
            System.out.println(token);
            return token;
        }catch (UnsupportedEncodingException exception){
            System.out.println(exception.toString());
            return exception.toString()+"error";
            //UTF-8 encoding not supported
        } catch (JWTCreationException exception){
            //Invalid Signing configuration / Couldn't convert Claims.
            System.out.println(exception.toString());
            return exception.toString()+"error";
        }
        catch(HibernateException ex){
            System.out.println(ex.toString());
            return ex.toString()+"error";
        }
    }
    
    @Override
    public String getToken(String id){
        Query q;
        q=getSession().createSQLQuery("SELECT `token` from `token` WHERE `idUser`=:id");
        q.setString("id",id);
        List list=q.list();
        String token=list.get(0).toString();
        return token;
    }

    
    @Override
    public String validateUser(String token,String id){
        DecodedJWT jwt;
        try {
            String secret="umsebenzisi";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println("valid "+encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);
            JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("http://socialclick.web.id")
                .withClaim("access","user").withClaim("id",id).withClaim("admin", Boolean.FALSE)    
                .build(); //Reusable verifier instance
            jwt = verifier.verify(token);
            Map<String, Claim> claims = jwt.getClaims();    //Key is the Claim name
            String claim = claims.get("access").toString();
            return "valid";
        } 
        catch (UnsupportedEncodingException exception){
            //UTF-8 encoding not supported
            System.out.println(exception.toString());
            return exception.toString();
        } 
        catch (InvalidClaimException exception){
            //Invalid signature/claims           
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/                 
            return "invalid claim";
        }
        catch (TokenExpiredException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "token expired";
        }
        catch(AlgorithmMismatchException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/                
            return "invalid algorithm used";
        }
        catch(JWTDecodeException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error decoding token";
        }
        catch(SignatureVerificationException ex){
           /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error verifying signature";
        }
    }
    
    @Override
    public void logout(String token,String id){
        /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
        //q.setString("id",id);
    }
    
   
    @Override
    public String returnId(String email){
            Query qUser;
            qUser=getSession().createSQLQuery("SELECT `id` from `user` WHERE `email`=:email");
            qUser.setString("email",email);
            List list=qUser.list();
            String id=list.get(0).toString();
            return id;
    }
    
    
    
    @Override
    public String validateValidationToken(String token,long id){
        DecodedJWT jwt;
        try {
            String secret="umsebenzisi";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println("valid "+encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);
            JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("http://socialclick.web.id")
                .withClaim("access","user").withClaim("admin", Boolean.FALSE)
                .withClaim("verified", Boolean.FALSE).withClaim("id", id)
                .build(); //Reusable verifier instance
            jwt = verifier.verify(token);
            Map<String, Claim> claims = jwt.getClaims();    //Key is the Claim name
            String claim = claims.get("access").toString();
            //change user status to "valid"
            Query q=getSession().createSQLQuery("UPDATE `user` set `status`=:status where `id`=:id");
            q.setParameter("status", 1);
            q.setParameter("id",3);
            q.executeUpdate();
            return "valid";
        } 
        catch (UnsupportedEncodingException exception){
            //UTF-8 encoding not supported
            System.out.println(exception.toString());
            return exception.toString()+"error";
        } 
        catch (InvalidClaimException exception){
            //Invalid signature/claims           
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/                 
            return "error: invalid claim";
        }
        catch (TokenExpiredException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error: token expired";
        }
        catch(AlgorithmMismatchException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/                
            return "error: invalid algorithm used";
        }
        catch(JWTDecodeException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error decoding token";
        }
        catch(SignatureVerificationException ex){
           /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error verifying signature";
        }
        catch(HibernateException ex){
            System.out.println(ex.toString());
            return "database error";
        }
    }
    
    @Override
    public String createToken(long id){
        try{
            long time=System.currentTimeMillis();
            String secret="umsebenzisi";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println(encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);   
            now=new Date(time);
            end=new Date(time+ 86400000);//1 day expiry time
            System.out.println(now);
            System.out.println(end);
            //idtoken=id;
            //emailtoken=emailverif;
            String token = JWT.create()
                .withIssuer(issuer).withIssuedAt(now).withExpiresAt(end)
                .withClaim("id", id).withClaim("access",access).withClaim("admin", Boolean.FALSE)
                .sign(algorithm);//create JWT token with issuer auth0, and claims of user's id in database
                //and access type of 'user'
            return token;
        }
        catch (UnsupportedEncodingException exception){
            //UTF-8 encoding not supported
            System.out.println(exception.toString());
            return exception.toString()+"error";
        } 
        catch (InvalidClaimException exception){
            //Invalid signature/claims           
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/                 
            return "error: invalid claim";
        }
        catch (TokenExpiredException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error: token expired";
        }
        catch(AlgorithmMismatchException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/                
            return "error: invalid algorithm used";
        }
        catch(JWTDecodeException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error decoding token";
        }
        catch(SignatureVerificationException ex){
           /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error verifying signature";
        }    
    }
    
    @Override
    public String validateResetToken(String token,long id){
        DecodedJWT jwt;
        try {
            String secret="umsebenzisi";
            String encoded= Base64.getUrlEncoder().encodeToString(secret.getBytes());
            System.out.println("valid "+encoded);
            Algorithm algorithm = Algorithm.HMAC256(encoded);
            JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("http://socialclick.web.id")
                .withClaim("access","user").withClaim("admin", Boolean.FALSE)
                .withClaim("id", id)
                .build(); //Reusable verifier instance
            jwt = verifier.verify(token);
            Map<String, Claim> claims = jwt.getClaims();    //Key is the Claim name
            String claim = claims.get("access").toString();
            //change user status to "valid"
            
            return "valid";
        } 
        catch (UnsupportedEncodingException exception){
            //UTF-8 encoding not supported
            System.out.println(exception.toString());
            return exception.toString()+"error";
        } 
        catch (InvalidClaimException exception){
            //Invalid signature/claims           
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/                 
            return "error: invalid claim";
        }
        catch (TokenExpiredException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error: token expired";
        }
        catch(AlgorithmMismatchException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/                
            return "error: invalid algorithm used";
        }
        catch(JWTDecodeException ex){
            /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error decoding token";
        }
        catch(SignatureVerificationException ex){
           /*Query q;
            q=getSession().createSQLQuery("DELETE from `token` where `token`=:token");
            q.setString("token", token);*/      
            return "error verifying signature";
        }
        catch(HibernateException ex){
            System.out.println(ex.toString());
            return "database error";
        }
    }
    
    @Override
    public String resetPassword(String email,String password){
        Query q;
        try{
            q=getSession().createSQLQuery("UPDATE `user` set `password`=:password where `email`=:email");
            q.setParameter("password",password);
            q.setParameter("email",email);
            q.executeUpdate();
            return "ok";
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            return ex.toString()+"error";
        }
    }
    
    @Override
    public String checkDOB(String dob){        
        Calendar hundredyears=Calendar.getInstance();//get date for 100 years ago
        hundredyears.add(Calendar.YEAR, -100);
        Date hundredyearsdate=hundredyears.getTime();//convert to date
        Calendar minAge=Calendar.getInstance();//get minimum age
        minAge.add(Calendar.YEAR,-10);
        Date minAgedate=minAge.getTime();
        
       try {           
           Date datedob=date.parse(dob);
           if(datedob.before(hundredyearsdate)){
               return "too old";
           }
           else if(datedob.after(minAgedate)){
               return "too young";
           }
           return "ok";
       } catch (ParseException ex) {
           Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
           return ex.toString()+"error";
       }
        
    }
    
    @Override
    public String rehash(String id){
        Query q=getSession().createSQLQuery("UPDATE `organization` set `password`=:password where `id`=:id");
        q.setParameter("password",id);
        q.setParameter("id","1");
        q.executeUpdate();
        q=getSession().createSQLQuery("SELECT `password` from `user` where `id`=:id");
        q.setParameter("id", "5");
        String pass=(String) q.list().get(0);
        if(!pass.equals(id)){
            return "invalid password error";
        }
        return "valid";
    }
}
