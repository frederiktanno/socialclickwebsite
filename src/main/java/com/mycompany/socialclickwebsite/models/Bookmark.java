/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.models;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 *
 * @author ASUS
 */
@Entity
@Table(name="\"bookmark\"",uniqueConstraints={@UniqueConstraint(columnNames={"idUser"})})
@Access(AccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Bookmark implements Serializable{
    @Id
    @Column(name="idBookmark")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    @OneToOne
    @JoinColumn(name="idUser",unique=true)
    private Users user;
    public Users getUser(){
        return user;
    }
    public void setUser(Users user){
        this.user=user;
    }
    
    @OneToMany(fetch=FetchType.LAZY)
    @JoinTable(name="bookmarkitems",
            joinColumns=@JoinColumn(name="idBookmark"),
            inverseJoinColumns=@JoinColumn(name="idProject")
    )
    @JsonProperty("projectList")
    private List<Project>projectList;
    public List<Project>getProjects(){
        return projectList;
    }
    public void setProjects(List<Project>projectList){
        this.projectList=projectList;
    }
}
