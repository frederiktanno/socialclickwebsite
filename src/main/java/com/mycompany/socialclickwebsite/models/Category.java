/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.models;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
//import java.util.HashSet;
import java.util.List;
//import java.util.Set;
//import java.util.HashSet;
//import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 *
 * @author ASUS
 */
@Entity
@Table(name="\"category\"",uniqueConstraints={@UniqueConstraint(columnNames="category")})
@Access(AccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Category implements Serializable{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    @Column(name="category",unique=true)
    private String category;
    public String getCategory(){
        return category;
    }
    
    public void setCategory(String category){
        this.category=category;
    }   
}
