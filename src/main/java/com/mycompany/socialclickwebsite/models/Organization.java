/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
//import java.util.HashSet;
import java.util.List;
//import java.util.Set;
//import java.util.HashSet;
//import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 *
 * @author ASUS
 */
@Entity
@Table(name="\"organization\"",uniqueConstraints={@UniqueConstraint(columnNames={"email","name"})})
@Access(AccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true,value="idAccess")
public class Organization implements Serializable{
     @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    @Column(name="name",unique=true)
    private String name;
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="idtype")
    private Orgtype orgtype;
    public Orgtype getType(){
        return orgtype;
    }
    public void setType(Orgtype orgtype){
        this.orgtype=orgtype;
    }
    
    @Column(name="phone")
    private String phone;
    public String getPhone(){
        return phone;
    }
    public void setPhone(String phone){
        this.phone=phone;
    }
    
    @Column(name="email",unique=true)
    private String email;
    public String getEmail(){
        return email;
    }
    public void setEmail(String email){
        this.email=email;
    }
    
    @Column(name="password")
    
    private String password;
    @JsonIgnore
    public String getPassword(){
        return password;
    }
    @JsonProperty
    public void setPassword(String password){
        this.password=password;
    }
    
    @Column(name="contact")
    private String contact;
    public String getContact(){
        return contact;
    }
    public void setContact(String contact){
        this.contact=contact;
    }
    
    @Column(name="verified")
    private int verified;
    public int getVerified(){
        return verified;
    }
    public void setVerified(int verified){
        this.verified=verified;
    }
    
    @Column(name="description")
    @JsonProperty("description")
    private String description;
    public String getDesc(){
        return description;
    }
    public void setDesc(String description){
        this.description=description;
    }

    @Column(name="founded")
    private int founded;
    public int getFounded(){
        return founded;
    }
    public void setFounded(int founded){
        this.founded=founded;
    }
    
    @Column(name="photo",nullable=true)
    private String photo;
    public String getPhoto(){
        return photo;
    }
    public void setPhoto(String photo){
        this.photo=photo;
    }
    @Column(name="rating")
    private float rating;
    public float getRating(){
        return rating;
    }
    public void setRating(float rating){
        this.rating=rating;
    }
    @Column(name="status")
    private int status;
    public int getStatus(){
        return status;
    }
    public void setStatus(int status){
        this.status=status;
    }
    
    @Column(name="contactphone")
    @JsonProperty("contactphone")
    private String cphone;
    public String getCphone(){
        return cphone;
    }
    public void setCphone(String cphone){
        this.cphone=cphone;
    }
    
    @Column(name="contactmail")
    @JsonProperty("contantemail")
    private String cemail;
    public String getCemail(){
        return cemail;
    }
    public void setCemail(String cemail){
        this.cemail=cemail;
    }
    
    @Override
    public String toString(){
        return(this.id +" "+ this.name+" "+this.description+" "+ this.email+" "+this.phone+" "+this.founded+" "+this.contact
                +" "+this.cemail+" "+this.cphone+" "+this.rating+" "+this.status+" "+this.verified);
    }
}
