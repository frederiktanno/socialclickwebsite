/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.models;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
//import java.util.HashSet;
import java.util.List;
//import java.util.Set;
//import java.util.HashSet;
//import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
/**
 *
 * @author ASUS
 */
@Entity
@Table(name="\"orgtype\"",uniqueConstraints={@UniqueConstraint(columnNames="type")})
@Access(AccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Orgtype implements Serializable {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
   
    @Column(name="type",unique=true)
    private String type;
    
    public int getId(){
        return id;
    }
    public void setId(int id){
        this.id=id;
    }
    
    public String getType(){
        return type;
    }
    public void setType(String type){
        this.type=type;
    }
    
    @Override
    public String toString(){
        return(this.id +" "+ this.type);
    }
}
