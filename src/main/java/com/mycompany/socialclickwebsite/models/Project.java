/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.springframework.boot.autoconfigure.security.SecurityProperties.User;

/**
 *
 * @author Marvint11
 */
@Entity
@Table(name="\"project\"",uniqueConstraints={@UniqueConstraint(columnNames="name")})
@Access(AccessType.FIELD)
@JsonAutoDetect(fieldVisibility = Visibility.ANY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Project implements Serializable{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
   
    @Column(name="name",unique=true)
    private String name;
  
    
    @Column(name="description")
    private String description;
    
    @Column(name="datetime")
    @JsonProperty("datetime")
    private String date;
    
    @Column(name="location")
    private String location;
    
    
    @Column(name="regdeadline")
    @JsonProperty("regdeadline")
    private String regdeadline;
    
    @Column(name="maxmembers",nullable=false)
    @JsonProperty("maxmembers")
    private int maxmembers;
    
    @Column(name="rating")
    private float rating;
    
    @Column(name="finished",nullable=false)
    private int finished;

    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDatetime() {
        return date;
    }

    public void setDatetime(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }


    public String getRegdeadline() {
        return regdeadline;
    }

    public void setRegdeadline(String regDeadline) {
        this.regdeadline = regDeadline;
    }

    public int getMaxmembers() {
        return maxmembers;
    }

    public void setMaxmembers(int maxMembers) {
        this.maxmembers = maxMembers;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    
    
    @OneToOne(cascade=CascadeType.ALL)
    @JoinTable(
            name="projectinitorg",
            joinColumns=@JoinColumn(name="idProject"),
            inverseJoinColumns=@JoinColumn(name="idOrg")
    )
   private Organization organization;
   public Organization getOrganization(){
       return organization;
   }
   public void setOrganization(Organization organization){
       this.organization=organization;
   }
    
    @OneToOne(cascade=CascadeType.ALL)
    @JoinTable(
            name="projectinituser",
            joinColumns=@JoinColumn(name="idProject"),
            inverseJoinColumns=@JoinColumn(name="idUser")
    )
   private Users users;
   public Users getUsers(){
       return users;
   }
   public void setUsers(Users users){
       this.users=users;
   }
   
   @OneToMany(mappedBy="project",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
   @JsonManagedReference
   
   private Set<ProjectMember>projMember=new HashSet<>();
   public Set<ProjectMember>getProjMember(){
       return projMember;
   }
   public void setProjMember(Set<ProjectMember>projMember){
       this.projMember=projMember;
   }
   
   @OneToOne
   @JoinColumn(name="idCategory")
   @JsonProperty("category")
   private Category category;
   public Category getCategory(){
       return category;
   }
   public void setCategory(Category category){
       this.category=category;
   }
   
   @OneToMany(mappedBy="project",fetch=FetchType.LAZY)
   @JsonIgnore
   private List<Gallery>gallery;
   public List<Gallery>getGallery(){
       return gallery;
   }
   public void setGallery(List<Gallery>gallery){
       this.gallery=gallery;
   }
   
   public int getFinished(){
       return finished;
   }
  public void setFinished(int finished){
      this.finished=finished;
  }
  
    @Column(name="profile",nullable=true)
    @JsonProperty("profile")
    private String profile;
    public String getProfile(){
        return profile;
    }
    public void setProfile(String photo){
        this.profile=photo;
    }
    
    @Column(name="hours")
    private int hours;
    public int getHours(){
        return this.hours;
    }
    public void setHours(int hours){
        this.hours=hours;
    }
   
    @Override
    public String toString(){
        return(this.id +" "+ this.name+" "+this.description+" "+this.location+" "+this.date+" "+this.organization
                +this.users+" "+this.regdeadline+" "+this.maxmembers+" "+this.rating);
    }
    
}