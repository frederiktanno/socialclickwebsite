/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.models;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Marvint11
 */
@Entity
@Table(name="\"projectmember\"")
@Access(AccessType.FIELD)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectMember implements Serializable{
    
    
    @Id
    @ManyToOne (cascade=CascadeType.ALL)
    @JoinColumn(name="idProject")
    @JsonBackReference
    private Project project;
    
    @Id
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="idUser")
    @JsonBackReference(value="user-projMember")
    private Users users;
    
    @Column(name="hours")
    private int hour;
    
    @Column(name="approved")
    private int approve;
    
    
    
    public Project getProject(){
        return project;
    }
    public void setProject(Project project){
        this.project=project;
    }
    
    public Users getMember(){
        return users;
    }
    public void setMember(Users user){
        this.users=user;
    }
    
    public int getHour(){
        return hour;
    }
    public void setHour(int hour){
        this.hour=hour;
    }
    
    public int getApprove(){
        return approve;
    }
    public void setApprove(int approve){
        this.approve=approve;
    }
    
    @Override
    public String toString(){
        return( this.project+ " "+this.users+" "+this.hour+" "+this.approve);
    }
}
