/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name="\"token\"")
@Access(AccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true,value={"user"})
public class Token implements Serializable{
     @Id
    @Column(name="id")
     @GeneratedValue(strategy=GenerationType.IDENTITY)
    private BigInteger id;
    public BigInteger getId() {
        return id;
    }
    
    public void setId(BigInteger id) {
        this.id = id;
    }
    
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="idUser",nullable=false)
    private Users user;
    public Users getUser(){
        return user;
    }
    public void setUser(Users user){
        this.user=user;
    }
    
    @Column(name="token")
    private String token;
    public String getToken(){
        return token;
    }
    public void setToken(String token){
        this.token=token;
    }
    
    @Column(name="validTime")
    String time;
    public String getTime(){
        return time;
    }
    public void setTime(String time){
        this.time=time;
    }
}
