package com.mycompany.socialclickwebsite.models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
//import java.util.HashSet;
//import java.util.Set;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
//import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.DynamicUpdate;
/**
 *
 * @author EdbertX
 */
@Entity
@Table(name="\"user\"",uniqueConstraints={@UniqueConstraint(columnNames={"email"})})
@Access(AccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Users implements Serializable{
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    public long getId(){
        return id;
    }
    public void setId(long id){
        this.id=id;
    }
    @Column(name="fname",nullable=false)
    private String fname;
    public String getfname(){
        return fname;
    }
    public void setfname(String fname){
        this.fname=fname;
    }
    
    @Column(name="lname",nullable=false)
    private String lname;
    public String getlname(){
        return lname;
    }
    public void setlname(String lname){
        this.lname=lname;
    }
    @Column(name="date_of_birth",nullable=false)
    @JsonProperty("dob")
    private String dob;
    public String getDOB(){
        return dob;
    }
    public void setDOB(String dob){
        this.dob=dob;
    }
    @Column(name="birthplace",nullable=false)
    private String birthplace;
    public String getBirthplace(){
        return birthplace;
    }
    public void setBirthplace(String birthplace){
        this.birthplace=birthplace;
    }
    @Column(name="gender",nullable=false)
    private String gender;
    public String getGender(){
        return gender;
    }
    public void setGender(String gender){
        this.gender=gender;
    }
    @Column(name="address",nullable=false)
    private String address;
    public String getAddress(){
        return address;
    }
    public void setAddress(String address){
        this.address=address;
    }
    @Column(name="nationality",nullable=false)
    private String nationality;
    public String getNationality(){
        return nationality;
    }
    public void setNationality(String nationality){
        this.nationality=nationality;
    }
    @Column(name="email", unique = true,nullable=false )
    private String email;
    public String getEmail(){
        return email;
    }
    public void setEmail(String email){
        this.email=email;
    }
    @Column(name="password",nullable=false)
    private String password;
    @JsonIgnore
    public String getPassword(){
        return password;
    }
    @JsonProperty
    public void setPassword(String password){
        this.password=password;
    }
    @Column(name="phone",nullable=false)
    private String phone;
    public String getPhone(){
        return phone;
    }
    public void setPhone(String phone){
        this.phone=phone;
    }
    @Column(name="statename",nullable=false)
    @JsonProperty("statename")//map JSON property name to the 'statename' field
    private String statename;
    public String getStateName(){
        return statename;
    }
    public void setStateName(String statename){
        this.statename=statename;
    }
    @Column(name="postcode",nullable=false)
    private int postcode;
    public int getPostcode(){
        return postcode;
    }
    public void setPostcode(int postcode){
        this.postcode=postcode;
    }
    @Column(name="lastedu",nullable=false)
    @JsonProperty("lastedu")
    private String lastedu;
    public String getLastedu(){
        return lastedu;
    }
    public void setLastedu(String lastedu){
        this.lastedu=lastedu;
    }
    @Column(name="occupation")
    private String occupation;
    public String getOccupation(){
        return occupation;
    }
    public void setOccupation(String occupation){
        this.occupation=occupation;
    }
    @Column(name="idnum",unique=true)
    private String idnum;
    public String getIdnum(){
        return idnum;
    }
    public void setIdnum(String idnum){
        this.idnum=idnum;
    }
    @Column(name="experience")
    private String experience;
    public String getExperience(){
        return experience;
    }
    public void setExperience(String experience){
        if(experience==null){
            this.experience=" ";
        }
        else{
          this.experience=experience;  
        }
        
    }
    @Column(name="description",nullable=true)
    private String description;
    public String getDescription(){
        return description;
    }
    public void setDescription(String description){
        this.description=description;
    }
    @Column(name="rating")
    private float rating;
    public float getRating(){
        return rating;
    }
    public void setRating(float rating){
        this.rating=rating;
    }
    @Column(name="status")
    private int status;
    public int getStatus(){
        return status;
    }
    public void setStatus(int status){
        this.status=status;
    }
    @Column(name="address2")
    private String address2;
    public String getAddress2(){
        return address2;
    }
    public void setAddress2(String address2){
        this.address2=address2;
    }
    @Column(name="address3")
    private String address3;
    public String getAddress3(){
        return address3;
    }
    public void setAddress3(String address3){
        this.address3=address3;
    }
    @OneToMany(mappedBy="users",fetch=FetchType.LAZY,cascade=CascadeType.ALL)
    @JsonManagedReference(value="user-projMember")
    @JsonIgnore
    private Set<ProjectMember> projMember=new HashSet<ProjectMember>();
    public Set<ProjectMember>getProjMember(){
       return projMember;
    }
   public void setProjMember(Set<ProjectMember>projMember){
       this.projMember=projMember;
    }
   
    @Column(name="photo",nullable=true)
    @JsonView(Users.class)
    private String photo;
    public String getPhoto(){
        return photo;
    }
    public void setPhoto(String photo){
        this.photo=photo;
    }
    
    @OneToMany(fetch=FetchType.EAGER)
    @JoinTable(
            name="userinterests",
            joinColumns=@JoinColumn(name="iduser"),
            inverseJoinColumns=@JoinColumn(name="idinterest")
    )
    @OrderBy("interest ASC")
    @JsonProperty("interest")
    private List<Interest>interest;
    public List<Interest> getInterest(){
        return interest;
    }
    public void setInterest(List<Interest>interest){
        this.interest=interest;
    }
    
    @Column(name="accessLevel",nullable=false)
    private int access;
    
    @JsonIgnore
    public int getAccess(){
        return access;
    }
    
    @JsonProperty
    public void setAccess(int access){
        this.access=access;
    }
    
    @Column(name="totalHours",nullable=false)
    private int totalHours;
    public int getHours(){
        return totalHours;
    }
    public void setHours(int hours){
        this.totalHours=hours;
    }
    
    @OneToOne(mappedBy="user",fetch=FetchType.LAZY)
    @JsonIgnore
    private Bookmark bookmark;
    public Bookmark getBookmark(){
        return bookmark;
    }
    public void setBookmark(Bookmark bookmark){
        this.bookmark=bookmark;
    }

    @Override
    public String toString(){
        return(this.id +" "+ this.fname+" "+this.lname+" "+this.gender+" "+this.email+" "+this.password+" "+this.phone
                +" "+this.nationality+" "+this.statename+" "+this.birthplace+" "+this.dob+" "+this.description+" "
                +this.lastedu+" "+this.occupation+" "+this.experience+" "+this.address+" "+this.address2+" "+this.address3
                +" "+this.idnum+" "+this.postcode+" "+this.rating+" "+this.status);
    } 
}
        




