/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.services;
import java.util.List;
import com.mycompany.socialclickwebsite.models.Project;
import com.mycompany.socialclickwebsite.models.Organization;
import com.mycompany.socialclickwebsite.models.Users;
/**
 *
 * @author ASUS
 */
public interface AdminServices {
    String validateOrg(String id);
    String regisAdmin(Users user);
    String loginAdmin(String email,String password);
    String banUser(String id);
}
