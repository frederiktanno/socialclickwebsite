/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.services;

import com.mycompany.socialclickwebsite.dao.AdminDao;
import com.mycompany.socialclickwebsite.models.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ASUS
 */
@Service("AdminServices")
@Transactional
public class AdminServicesImpl implements AdminServices{
    @Autowired
    private AdminDao aDao;
    
    @Override
    public String validateOrg(String id){
        return aDao.validateOrg(id);
    }
    
    @Override
    public String regisAdmin(Users user){
        return aDao.regisAdmin(user);
    }
    
    @Override
    public String loginAdmin(String email,String password){
        return aDao.loginAdmin(email, password);
    }
    
    @Override
    public String banUser(String id){
        return aDao.banUser(id);
    }
}
