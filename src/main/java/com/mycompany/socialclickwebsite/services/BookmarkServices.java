/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;
import com.mycompany.socialclickwebsite.models.Bookmark;
import com.mycompany.socialclickwebsite.models.Project;
import java.util.List;

/**
 *
 * @author Jason Kusuma
 */
public interface BookmarkServices {
    List<Project> getBookmarkById(String id);
//    List<Bookmark> getBookmark();
    String saveBookmark(Bookmark bookmark);
    void updateBookmark(Bookmark bm);
    void deleteBookmarkById(long id);
    Boolean getBookmark(String idUser, String idProject);
    
}
