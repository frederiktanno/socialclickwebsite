/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;
import com.mycompany.socialclickwebsite.dao.BookmarkDao;
import com.mycompany.socialclickwebsite.models.Bookmark;
import com.mycompany.socialclickwebsite.models.Project;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Jason Kusuma
 */
@Service("BookmarkServices")
@Transactional
public class BookmarkServicesImpl implements BookmarkServices{
    @Autowired
    private BookmarkDao dao;
    
    @Override
    public List<Project> getBookmarkById(String id){
        return dao.getBookmarkById(id);
    }
    
//    @Override
//    public List<Bookmark>getBookmark(){
//        return dao.getBookmark();
//      
//    }
    
    @Override
    public String saveBookmark(Bookmark bookmark){
        return dao.saveBookmark(bookmark);
    }
    
    @Override
    public Boolean getBookmark(String idUser, String idProject){
        return dao.getBookmark(idUser, idProject);
    }
    
    @Override
    public void updateBookmark(Bookmark bm){
        dao.updateBookmark(bm);
    }
    
    @Override
    public void deleteBookmarkById(long id){
        dao.deleteBookmarkById(id);
    }
}
