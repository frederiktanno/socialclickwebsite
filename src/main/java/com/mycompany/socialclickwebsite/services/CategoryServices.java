/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;
import com.mycompany.socialclickwebsite.models.Category;
import java.util.List;
/**
 *
 * @author ASUS
 */
public interface CategoryServices {
    Category getCategoryById(String id);
//    List<Category> getCategory();
//    Category getCategoryByName(String name);
    void saveCategory(Category interest);
}
