/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;
import com.mycompany.socialclickwebsite.dao.CategoryDao;
import com.mycompany.socialclickwebsite.models.Category;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Service("CategoryServices")
@Transactional
public class CategoryServicesImpl implements CategoryServices{
    @Autowired
    private CategoryDao dao;
    
    @Override
    public Category getCategoryById(String id){
        return dao.getCategoryById(Integer.parseInt(id));
    }
    
//    @Override
//    public List<Category> getCategory(){
//        return dao.getCategory();
//    }
//    
//    @Override
//    public Category getCategoryByName(String name){
//        return dao.getCategoryByName(name);
//    }
    
    @Override
    public void saveCategory(Category category){
        dao.saveCategory(category);
    }
}
