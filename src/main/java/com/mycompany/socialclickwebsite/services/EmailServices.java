/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.services;

/**
 *
 * @author ASUS
 */
public interface EmailServices {
    String sendEmail(String token,String email);
}
