/*
 * SocialClickWebsiteProject by Live und Learn Team
 */
package com.mycompany.socialclickwebsite.services;

import com.mycompany.socialclickwebsite.dao.EmailDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ASUS
 */
@Service("EmailServices")
public class EmailServicesImpl implements EmailServices{
    @Autowired
    private EmailDao eDao;
    @Override
    public String sendEmail(String token,String email){
        return eDao.sendEmail(token,email);
    }
}
