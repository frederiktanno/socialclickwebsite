/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;
import com.mycompany.socialclickwebsite.models.Gallery;
import java.util.List;
/**
 *
 * @author ASUS
 */
public interface GalleryServices {
     List<Gallery>getGallery();
     void saveGallery(Gallery gallery);
}
