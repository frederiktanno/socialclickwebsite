/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;
import com.mycompany.socialclickwebsite.models.Gallery;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mycompany.socialclickwebsite.dao.GalleryDao;
/**
 *
 * @author ASUS
 */
@Service("GalleryServices")
@Transactional
public class GalleryServicesImpl implements GalleryServices{
    @Autowired
    private GalleryDao gDao;
    
    @Override
    public void saveGallery(Gallery gallery){
        gDao.saveGallery(gallery);
    }
    
    @Override
    public List<Gallery>getGallery(){
        return gDao.getGallery();
    }
}
