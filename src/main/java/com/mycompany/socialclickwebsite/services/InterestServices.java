/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;

import com.mycompany.socialclickwebsite.models.Interest;
import java.util.List;

/**
 *
 * @author Marvint11
 */
public interface InterestServices {
    Interest getInterestById(String id);
//    List<Interest> getInterest();
//    Interest getInterestByName(String name);
    void saveInterest(Interest interest);
}
