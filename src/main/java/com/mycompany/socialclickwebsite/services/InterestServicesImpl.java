/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;

import com.mycompany.socialclickwebsite.dao.InterestDao;
import com.mycompany.socialclickwebsite.models.Interest;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marvint11
 */
@Service("InterestServices")
@Transactional
public class InterestServicesImpl implements InterestServices{
    
    @Autowired
    private InterestDao dao;
    
    @Override
    public Interest getInterestById(String id){
        return dao.getInterestById(Integer.parseInt(id));
    }
    
//    @Override
//    public List<Interest> getInterest(){
//        return dao.getInterest();
//    }
//    
//    @Override
//    public Interest getInterestByName(String name){
//        return dao.getInterestByName(name);
//    }
    
    @Override
    public void saveInterest(Interest interest){
        dao.saveInterest(interest);
    }
}
