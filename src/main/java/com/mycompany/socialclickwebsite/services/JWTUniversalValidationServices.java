/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;


/**
 *
 * @author ASUS
 */
public interface JWTUniversalValidationServices {
    String validateToken(String token,String id);
    String validateAdmin(String token,String id);
    String getClaim(String token);
}
