/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;

import com.mycompany.socialclickwebsite.dao.JWTUniversalValidationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
/**
 *
 * @author ASUS
 */
@Service("JWTUniversalValidationServices")
@Transactional
public class JWTUniversalValidationServicesImpl implements JWTUniversalValidationServices{
    @Autowired
    private JWTUniversalValidationDao jDao;
    
    @Override
    public String validateToken(String token, String id){
        return jDao.validateToken(token, id);
    }
    @Override
    public String validateAdmin(String token,String id){
        return jDao.validateAdmin(token, id);
    }
    
    @Override
    public String getClaim(String token){
        return jDao.getClaim(token);
    }
}
