/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;

import com.mycompany.socialclickwebsite.models.Organization;
import com.mycompany.socialclickwebsite.models.Token;
import java.util.List;

/**
 *
 * @author Marvint11
 */
public interface OrganizationServices {
    void saveOrganization(Organization organization);
    List<Organization>getOrganization();
    Organization getOrganizationById(String id);
    Organization getOrganizationByName(String name);
    Organization getOrganizationByEmail(String email);
    boolean checkOrgEmail(String email,String id);
    boolean checkOrgName(String name, String id);
    void updateOrganization(Organization organization);
    void deleteOrganizationById(String id);
    void setVerification(String id,String verification);
    void setStatus(String id,String status);
    boolean isOrgExist(Organization org);
    String loginAuth(String email, String password);
    String regisAuth(Organization org);
    String getToken(String id);
    String returnId(String email);
    String validateOrg(String token,String id);
}
