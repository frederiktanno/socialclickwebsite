/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;

import com.mycompany.socialclickwebsite.controllers.PasswordHash;
import com.mycompany.socialclickwebsite.dao.OrganizationDao;
import com.mycompany.socialclickwebsite.models.Organization;
import com.mycompany.socialclickwebsite.models.Token;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marvint11
 */
@Service("OrganizationServices")
@Transactional
public class OrganizationServicesImpl implements OrganizationServices{
    PasswordHash ph=new PasswordHash();
    @Autowired
    private OrganizationDao dao;
    
    @Override
    public void saveOrganization(Organization organization){
        String pass=organization.getPassword();
        pass=BCrypt.hashpw(pass, BCrypt.gensalt());
        organization.setPassword(pass);
        dao.saveOrganization(organization);
    }
    
    @Override
    public List<Organization>getOrganization(){
        return dao.getOrganization();
    }
    
    @Override
    public Organization getOrganizationById(String id){
        return dao.getOrganizationById(Long.parseLong(id));
    }
    
    @Override
    public Organization getOrganizationByName(String name){
        return dao.getOrganizationByName(name);
    }
    
    @Override
    public boolean checkOrgEmail(String email,String id){
        return dao.checkOrgEmail(email, id);
    }
    
    @Override
    public boolean checkOrgName(String name,String id){
        return dao.checkOrgName(name, id);
    }
    
    @Override
    public void updateOrganization(Organization organization){
        dao.updateOrganization(organization);
    }
    
    @Override
    public void deleteOrganizationById(String id){
        dao.deleteOrganizationById(id);
    }
    
    @Override
    public void setVerification(String id,String verification){
        dao.setVerification(id, verification);
    }
    
    @Override
    public void setStatus(String id,String status){
        dao.setStatus(id, status);
    }
    
    @Override
    public Organization getOrganizationByEmail(String email){
        return dao.getOrganizationByEmail(email);
    }
    
    @Override
    public boolean isOrgExist(Organization org){
        return getOrganizationByEmail(org.getEmail())!=null || getOrganizationByName(org.getName())!=null;
    }
    
     @Override
    public String loginAuth(String email, String password){
        //String pass=user.getPassword();
        return dao.loginAuth(email,password);
    }    
    
    @Override
    public String regisAuth(Organization org){
        return dao.regisAuth(org);
    }
    
    @Override
    public String getToken(String id){
        return dao.getToken(id);
    }
    
    @Override
    public String validateOrg(String token,String id){
        return dao.validateOrg(token,id);
    }
    
    @Override
    public String returnId(String email){
        return dao.returnId(email);
    }
}
