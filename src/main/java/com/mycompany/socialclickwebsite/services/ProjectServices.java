/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;

import com.mycompany.socialclickwebsite.models.Gallery;
import com.mycompany.socialclickwebsite.models.Project;
import com.mycompany.socialclickwebsite.models.ProjectMember;
import java.util.List;

/**
 *
 * @author Marvint11
 */
public interface ProjectServices {
    String saveUserProject(Project project,String id);
    String saveOrgProject(Project project, String id);
    List<Project>getProject();
    List<Project>getOrgInitiatedProject(String id);
    Project getProjectById(String id);
    Object getProjectByName(String name);
    String updateProject(Project project);
    void deleteProjectById(String id);
    List<Project>getJoinedProject(String id);
    List<Project> getUserIniatedProject(String id);
    String joinProject(String id,String idProject);
    int countProjectMembers(String id);
    List<Gallery>getProjectGallery(String id);
    String approveApplicant(String id,String idProject);
    String rejectApplicant(String id, String idProject);
    int countAppliedUsers(String idProject);
    String setProjectStatus(String idProject);
    int countUserCreatedProjects(String id);
    int countOrgCreatedProjects(String id);
    int countJoinedProjects(String id);
    int countTotalHours(String id);
    List<Project>getProjectsByCategory(String[] idCategory);
     List<ProjectMember>getProjectMembers(String id);
     String finishProject(String id,int hours);
}
