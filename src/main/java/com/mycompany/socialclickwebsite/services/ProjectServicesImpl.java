/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;

import com.mycompany.socialclickwebsite.dao.ProjectDao;
import com.mycompany.socialclickwebsite.models.Gallery;
import com.mycompany.socialclickwebsite.models.Project;
import com.mycompany.socialclickwebsite.models.ProjectMember;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marvint11
 */
@Service("ProductServices")
@Transactional
public class ProjectServicesImpl implements ProjectServices{
    
    @Autowired
    private ProjectDao dao;
    
    @Override
    public String saveUserProject(Project project,String id){
        return dao.saveUserProject(project,id);
    }
    
    @Override
    public String saveOrgProject(Project project,String id){
        return dao.saveOrgProject(project,id);
    }
    
    @Override
    public List<Project>getProject(){
        return dao.getProject();
    }
    
    @Override
    public Project getProjectById(String id){
        return dao.getProjectById(Long.parseLong(id));
    }
    
    @Override
    public Object getProjectByName(String name){
        return dao.getProjectByName(name);
    }
    
    @Override
    public String updateProject(Project project){
        return dao.updateProject(project);
    }
    
    @Override
    public void deleteProjectById(String id){
        dao.deleteProjectById(id);
    }
    @Override
    public List<Project>getJoinedProject(String id){
        return dao.getJoinedProject(id);
    }
    
    @Override
    public List<Project> getUserIniatedProject(String id){
        return dao.getUserIniatedProject(id);
    }
    
    @Override
    public String joinProject(String id,String idProject){
        return dao.joinProject(id, idProject);
    }
    
    @Override
    public int countProjectMembers(String id){
        return dao.countProjectMembers(id);
    }
    
    @Override
    public List<Gallery>getProjectGallery(String id){
        return dao.getProjectGallery(id);
    }
    
    @Override
    public List<Project>getOrgInitiatedProject(String id){
        return dao.getOrgInitiatedProject(id);
    }
    
    @Override
    public String approveApplicant(String id,String idProject){
        return dao.approveApplicant(id, idProject);
    }
    
    @Override
    public String rejectApplicant(String id,String idProject){
        return dao.rejectApplicant(id, idProject);
    }
    
    @Override
    public int countAppliedUsers(String idProject){
        return dao.countAppliedUsers(idProject);
    }
    @Override
    public String setProjectStatus(String idProject){
        return dao.setProjectStatus(idProject);
    }
    
    @Override
    public int countUserCreatedProjects(String id){
        return dao.countUserCreatedProjects(id);
    }
    
    @Override
    public int countOrgCreatedProjects(String id){
        return dao.countOrgCreatedProjects(id);
    }
    
    @Override
    public int countJoinedProjects(String id){
        return dao.countJoinedProjects(id);
    }
    
    @Override
    public int countTotalHours(String id){
        return dao.countTotalHours(id);
    }
    
    @Override
    public List<Project>getProjectsByCategory(String[]idCategory){
        return dao.getProjectsByCategory(idCategory);
    }
    @Override
    public List<ProjectMember>getProjectMembers(String id){
        return dao.getProjectMembers(id);
    }
    
    @Override
    public String finishProject(String id,int hours){
        return dao.finishProject(id,hours);
    }
}
