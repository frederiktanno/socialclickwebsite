/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;
import com.mycompany.socialclickwebsite.models.Bookmark;
import com.mycompany.socialclickwebsite.models.Project;
import com.mycompany.socialclickwebsite.models.Users;
import java.util.List;

/**
 *
 * @author Marvint11
 */
public interface UserServices {
    String insertData(Users user);
    List<Users>getUser();
    void deleteUserById(String id);
    void updateUser(Users data);
    Users findByName(String name);
    Users findById(String id);
    boolean isUserExist(String email);
    boolean checkEmail(String email,String id);
    String loginAuth(String email, String password);
    String regisAuth(Users user);
    String getToken(String id);
    String returnId(String email);
    String validateUser(String token,String id);
    void logout(String token, String id);
    String checkDOB(String dob);
    String validateValidationToken(String token,long id);
    String createToken(long id);
    boolean checkIdNum(String idnum);
    String validateResetToken(String token,long id);
    String resetPassword(String email,String password);
    String rehash(String id);
}
