/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.socialclickwebsite.services;


import java.util.List;
import com.mycompany.socialclickwebsite.controllers.PasswordHash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mycompany.socialclickwebsite.models.Users;
import com.mycompany.socialclickwebsite.dao.UserDao;
import com.mycompany.socialclickwebsite.models.Bookmark;
import com.mycompany.socialclickwebsite.models.Project;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author Marvint11
 */
@Service("UserServices")
@Transactional
public class UserServicesImpl implements UserServices{
    PasswordHash ph=new PasswordHash();   
    
    @Autowired
    public UserDao uDao;

    @Override
    public List<Users>getUser(){
        return uDao.getAllData();
    }
    @Override
    public String insertData(Users user){
        /*Users u=new Users();
        u.setUsername(username);
        u.setPassword(password);
        u.setEmail(email);
        uDao.saveData(u);*/ 
        //System.out.println("TEST : ");
        //System.out.println(user.getAccess());
        //user.setPassword(passwordEncoder.encode(user.getPassword()));
        String pass=user.getPassword();
        pass=BCrypt.hashpw(pass, BCrypt.gensalt());
        user.setPassword(pass);
        return uDao.saveData(user);
    }
    @Override
    public Users findByName(String name){
        return uDao.findByName(name);
    }
    @Override
    public Users findById(String id){
        return uDao.findById(Long.parseLong(id));
    }
    @Override
    public boolean checkEmail(String email,String id){
        return uDao.checkEmail(email, id);
    }
    @Override
    public boolean checkIdNum(String idnum){
        return uDao.checkIdNum(idnum);
    }
    
    @Override
    public boolean isUserExist(String email){
        return findByName(email)!=null;
    }
    
    @Override
    public void deleteUserById(String id){
        uDao.deleteUserById(id);
    }
    
    @Override
    public void updateUser(Users user){
        //user.setPassword(passwordEncoder.encode(user.getPassword()));
        uDao.updateUser(user);
    }
    
    
    //@Override
    //public Users findByEmail(String user){
    //    return userRepository.findbyEmail(user);
    //}
    @Override
    public String loginAuth(String email, String password){
        //String pass=user.getPassword();

        return uDao.loginAuth(email,password);
    }    
    
    @Override
    public String regisAuth(Users user){
        return uDao.regisAuth(user);
    }
    
    @Override
    public String getToken(String id){
        return uDao.getToken(id);
    }
    
    
    @Override
    public String validateUser(String token,String id){
        return uDao.validateUser(token,id);
    }
    
    @Override
    public void logout(String token,String id){
         uDao.logout(token,id);
    }
    
    
    @Override
    public String returnId(String email){
        return uDao.returnId(email);
    }
    
    @Override
    public String validateValidationToken(String token,long id){
        return uDao.validateValidationToken(token,id);
    }
    
    @Override
    public String createToken(long id){
        return uDao.createToken(id);
    }
    
    @Override
    public String validateResetToken(String token,long id){
        return uDao.validateResetToken(token, id);
    }
    
    @Override
    public String resetPassword(String email,String password){
        try {
            password=BCrypt.hashpw(password, BCrypt.gensalt());
            return uDao.resetPassword(email, password);
        } catch (Exception ex) {
            Logger.getLogger(UserServicesImpl.class.getName()).log(Level.SEVERE, null, ex);
            return ex.toString()+"error";
        }
    }
    
    @Override
    public String checkDOB(String dob){
        return uDao.checkDOB(dob);
    }
    
    @Override
    public String rehash(String id){
        try {
            id=BCrypt.hashpw(id, BCrypt.gensalt());
            System.out.println(id);
            String pass="1234";
            pass=BCrypt.hashpw(pass, BCrypt.gensalt());
            System.out.println(pass);
            return uDao.rehash(id);
        } catch (Exception ex) {
            Logger.getLogger(UserServicesImpl.class.getName()).log(Level.SEVERE, null, ex);
            return ex.toString()+"error";
        }
    }
}
